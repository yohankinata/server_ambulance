-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 25 Jul 2017 pada 06.57
-- Versi Server: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mydb`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `ambulance`
--

CREATE TABLE `ambulance` (
  `idAmbulance` int(11) NOT NULL,
  `idAuth` int(11) NOT NULL,
  `idTypeAmbulance` int(11) DEFAULT NULL,
  `longitudeGmaps` varchar(255) DEFAULT NULL,
  `latitudeGmaps` varchar(255) DEFAULT NULL,
  `handphone` varchar(45) DEFAULT NULL,
  `nomorPolisi` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `notes` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `api_token`
--

CREATE TABLE `api_token` (
  `idAuth` int(11) NOT NULL,
  `API_Token` text NOT NULL,
  `createdDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `asam_urat`
--

CREATE TABLE `asam_urat` (
  `idAsam_Urat` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `nilai` decimal(10,2) DEFAULT NULL,
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `authentication`
--

CREATE TABLE `authentication` (
  `idAuthentication` int(11) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `category`
--

CREATE TABLE `category` (
  `idCategory` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `class_ambulance`
--

CREATE TABLE `class_ambulance` (
  `idclassAmbulance` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `document`
--

CREATE TABLE `document` (
  `idDocument` int(11) NOT NULL,
  `idVolunteer` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` text,
  `path` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `gula_darah`
--

CREATE TABLE `gula_darah` (
  `idGula_Darah` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `nilai` decimal(10,2) DEFAULT NULL,
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `hemoglobin`
--

CREATE TABLE `hemoglobin` (
  `idHemoglobin` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `nilai` decimal(10,2) DEFAULT NULL,
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kolesterol`
--

CREATE TABLE `kolesterol` (
  `idKolesterol` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `nilai` decimal(10,2) DEFAULT NULL,
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `notification`
--

CREATE TABLE `notification` (
  `idnotification` int(11) NOT NULL,
  `createdDate` varchar(45) DEFAULT NULL,
  `createdTime` varchar(45) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `idAuth` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `order`
--

CREATE TABLE `order` (
  `idOrder` int(11) NOT NULL,
  `idAmbulance` int(11) DEFAULT NULL,
  `idVolunteer` int(11) DEFAULT NULL,
  `idUser` int(11) DEFAULT NULL,
  `createdTime` datetime DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `notes_user` text,
  `price` decimal(10,2) DEFAULT NULL,
  `paymentStatus` varchar(45) DEFAULT NULL,
  `destination` varchar(45) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `notesAmbulanceVolunteer` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `post`
--

CREATE TABLE `post` (
  `idPost` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `postDate` datetime DEFAULT NULL,
  `content` text,
  `createdTime` datetime DEFAULT NULL,
  `createdBy` datetime DEFAULT NULL,
  `updateTime` datetime DEFAULT NULL,
  `updateBy` datetime DEFAULT NULL,
  `author` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `post_to_category`
--

CREATE TABLE `post_to_category` (
  `idCategory` int(11) NOT NULL,
  `idPost` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tekanan_darah`
--

CREATE TABLE `tekanan_darah` (
  `idTekanan_Darah` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `sistol` decimal(10,2) DEFAULT NULL,
  `diastol` decimal(10,2) DEFAULT NULL,
  `nadi` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `type_ambulance`
--

CREATE TABLE `type_ambulance` (
  `idTypeAmbulance` int(11) NOT NULL,
  `idClassAmbulance` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `iduser` int(11) NOT NULL,
  `idAuth` int(11) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `displayName` varchar(45) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `dateofBirth` datetime DEFAULT NULL,
  `golonganDarah` varchar(45) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `tinggiBadan` decimal(10,0) DEFAULT NULL,
  `riwayatKesehatan` text,
  `handphone` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `volunteer`
--

CREATE TABLE `volunteer` (
  `idVolunteer` int(11) NOT NULL,
  `idAuth` int(11) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `displayName` varchar(45) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `dateofBirth` datetime DEFAULT NULL,
  `golonganDarah` varchar(45) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `tinggiBadan` decimal(10,2) DEFAULT NULL,
  `riwayatPenyakit` text,
  `handphone` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ambulance`
--
ALTER TABLE `ambulance`
  ADD PRIMARY KEY (`idAmbulance`),
  ADD KEY `AmbulanceToType_idx` (`idTypeAmbulance`),
  ADD KEY `auth_idx` (`idAuth`);

--
-- Indexes for table `api_token`
--
ALTER TABLE `api_token`
  ADD KEY `auth_API_idx` (`idAuth`);

--
-- Indexes for table `asam_urat`
--
ALTER TABLE `asam_urat`
  ADD PRIMARY KEY (`idAsam_Urat`),
  ADD KEY `au_to_user_idx` (`idUser`);

--
-- Indexes for table `authentication`
--
ALTER TABLE `authentication`
  ADD PRIMARY KEY (`idAuthentication`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`idCategory`);

--
-- Indexes for table `class_ambulance`
--
ALTER TABLE `class_ambulance`
  ADD PRIMARY KEY (`idclassAmbulance`);

--
-- Indexes for table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`idDocument`),
  ADD KEY `doc_to_volunteer_idx` (`idVolunteer`);

--
-- Indexes for table `gula_darah`
--
ALTER TABLE `gula_darah`
  ADD PRIMARY KEY (`idGula_Darah`),
  ADD KEY `gd_to_user_idx` (`idUser`);

--
-- Indexes for table `hemoglobin`
--
ALTER TABLE `hemoglobin`
  ADD PRIMARY KEY (`idHemoglobin`),
  ADD KEY `hg_to_user_idx` (`idUser`);

--
-- Indexes for table `kolesterol`
--
ALTER TABLE `kolesterol`
  ADD PRIMARY KEY (`idKolesterol`),
  ADD KEY `kl_to_user_idx` (`idUser`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`idnotification`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`idOrder`),
  ADD KEY `ambulance_idx` (`idAmbulance`),
  ADD KEY `volunteer_idx` (`idVolunteer`),
  ADD KEY `order_to_user_idx` (`idUser`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`idPost`);

--
-- Indexes for table `post_to_category`
--
ALTER TABLE `post_to_category`
  ADD PRIMARY KEY (`idCategory`,`idPost`),
  ADD KEY `categorytopost_idx` (`idPost`);

--
-- Indexes for table `tekanan_darah`
--
ALTER TABLE `tekanan_darah`
  ADD PRIMARY KEY (`idTekanan_Darah`),
  ADD KEY `td_to_user_idx` (`idUser`);

--
-- Indexes for table `type_ambulance`
--
ALTER TABLE `type_ambulance`
  ADD PRIMARY KEY (`idTypeAmbulance`),
  ADD KEY `typetoclass_idx` (`idClassAmbulance`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`iduser`),
  ADD KEY `auth_idx` (`idAuth`);

--
-- Indexes for table `volunteer`
--
ALTER TABLE `volunteer`
  ADD PRIMARY KEY (`idVolunteer`),
  ADD KEY `auth_idx` (`idAuth`);

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `ambulance`
--
ALTER TABLE `ambulance`
  ADD CONSTRAINT `AmbulanceToType` FOREIGN KEY (`idTypeAmbulance`) REFERENCES `type_ambulance` (`idTypeAmbulance`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ambulance_to_auth` FOREIGN KEY (`idAuth`) REFERENCES `authentication` (`idAuthentication`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `api_token`
--
ALTER TABLE `api_token`
  ADD CONSTRAINT `api_to_auth` FOREIGN KEY (`idAuth`) REFERENCES `authentication` (`idAuthentication`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `asam_urat`
--
ALTER TABLE `asam_urat`
  ADD CONSTRAINT `au_to_user` FOREIGN KEY (`idUser`) REFERENCES `user` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `document`
--
ALTER TABLE `document`
  ADD CONSTRAINT `doc_to_volunteer` FOREIGN KEY (`idVolunteer`) REFERENCES `volunteer` (`idVolunteer`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `gula_darah`
--
ALTER TABLE `gula_darah`
  ADD CONSTRAINT `gd_to_user` FOREIGN KEY (`idUser`) REFERENCES `user` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `hemoglobin`
--
ALTER TABLE `hemoglobin`
  ADD CONSTRAINT `hg_to_user` FOREIGN KEY (`idUser`) REFERENCES `user` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `kolesterol`
--
ALTER TABLE `kolesterol`
  ADD CONSTRAINT `kl_to_user` FOREIGN KEY (`idUser`) REFERENCES `user` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `order_to_user` FOREIGN KEY (`idUser`) REFERENCES `user` (`iduser`) ON UPDATE CASCADE,
  ADD CONSTRAINT `order_to_volunteer` FOREIGN KEY (`idVolunteer`) REFERENCES `volunteer` (`idVolunteer`) ON UPDATE CASCADE,
  ADD CONSTRAINT `otder_to_ambulance` FOREIGN KEY (`idAmbulance`) REFERENCES `ambulance` (`idAmbulance`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `post_to_category`
--
ALTER TABLE `post_to_category`
  ADD CONSTRAINT `categorytocategory` FOREIGN KEY (`idCategory`) REFERENCES `category` (`idCategory`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `categorytopost` FOREIGN KEY (`idPost`) REFERENCES `post` (`idPost`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tekanan_darah`
--
ALTER TABLE `tekanan_darah`
  ADD CONSTRAINT `td_to_user` FOREIGN KEY (`idUser`) REFERENCES `user` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `type_ambulance`
--
ALTER TABLE `type_ambulance`
  ADD CONSTRAINT `typetoclass` FOREIGN KEY (`idClassAmbulance`) REFERENCES `class_ambulance` (`idclassAmbulance`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_to_auth` FOREIGN KEY (`idAuth`) REFERENCES `authentication` (`idAuthentication`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `volunteer`
--
ALTER TABLE `volunteer`
  ADD CONSTRAINT `volunteer_to_auth` FOREIGN KEY (`idAuth`) REFERENCES `authentication` (`idAuthentication`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
