-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 12 Okt 2017 pada 11.07
-- Versi Server: 10.0.32-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `marketk1_kincat`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `ambulance`
--

CREATE TABLE `ambulance` (
  `idAmbulance` int(11) NOT NULL,
  `longitudeGmaps` varchar(255) DEFAULT NULL,
  `latitudeGmaps` varchar(255) DEFAULT NULL,
  `handphone` varchar(45) DEFAULT NULL,
  `nomorPolisi` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `price` decimal(10,2) NOT NULL,
  `description` text NOT NULL,
  `notes` varchar(45) DEFAULT NULL,
  `idClass` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ambulance`
--

INSERT INTO `ambulance` (`idAmbulance`, `longitudeGmaps`, `latitudeGmaps`, `handphone`, `nomorPolisi`, `status`, `price`, `description`, `notes`, `idClass`) VALUES
(1, '106.82882309', '-6.44358853', '123123', 'B69123ZPE', '1', '700000.00', 'Yeah!', NULL, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `api_token`
--

CREATE TABLE `api_token` (
  `API_Token` text NOT NULL,
  `idAuth` int(11) NOT NULL,
  `createdDate` datetime DEFAULT NULL,
  `idToken` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `api_token`
--

INSERT INTO `api_token` (`API_Token`, `idAuth`, `createdDate`, `idToken`) VALUES
('d27426a015e275e5d102786ae3212a8f', 1, '2017-09-02 11:27:32', 1),
('6f3584c23ae795ca3501255b6396234a', 1, '2017-09-02 11:35:14', 2),
('0752a0709ea6f61c122f4bc5fe2b6190', 1, '2017-09-02 11:58:47', 3),
('7eebdd896c37222cdd0970c710975445', 1, '2017-09-02 12:00:54', 4),
('f015841065ed0274d1e12cbb688cdb8b', 1, '2017-09-02 12:06:24', 5),
('86b727d68757bc13a412290e33bd4df7', 1, '2017-09-02 12:09:17', 6),
('9216e38c79c385227aad061af09d2d23', 1, '2017-09-02 12:21:41', 7),
('66493beba447c350fb9ea6a5f941ae95', 1, '2017-09-02 12:23:35', 8),
('514a86f0217bd5b0fd7d9cadecf728d1', 1, '2017-09-02 12:32:06', 9),
('c214e56614d500624950fde89081dfbe', 1, '2017-09-02 12:56:52', 10),
('4693c05cf76b4451ab520c0c5d887c44', 2, '2017-09-02 13:00:00', 11),
('cb8149110b1ff220002f5d98e5878515', 1, '2017-09-02 13:00:21', 12),
('089063e19134a793aaea0cb3004d5f62', 2, '2017-09-02 13:00:34', 13),
('fa696167e7aa8049643307bad740bfe4', 1, '2017-09-02 13:02:37', 14),
('d89796de7ce8946d25c5efa7c7296d68', 1, '2017-09-02 13:10:31', 15),
('c7084daa0a845a8c95ef39491c15cd83', 2, '2017-09-02 13:20:17', 16),
('ddf94f84f498fd790e13c30330e8716b', 1, '2017-09-02 13:20:50', 17),
('8ad56406fffa7fd24dd6192d0b8b8216', 1, '2017-09-02 13:26:22', 18),
('edecdb0ae489dbaa6e5b495e76e08cb1', 2, '2017-09-02 13:42:08', 19),
('3bbfb757c6fa65c87cedffb8ffede024', 2, '2017-09-02 13:43:09', 20),
('2909f53854fa94c1773a6c1d43cbd06a', 1, '2017-09-02 22:30:56', 21),
('f973c05503d8276853fd88e14e465c91', 1, '2017-09-03 09:55:55', 22),
('5ab109ed80e1a09568741e5e171521b4', 2, '2017-09-03 15:18:39', 23),
('8e8c15b98a214f67abbee787a63b4eca', 2, '2017-09-03 16:25:12', 24),
('a0d7024519efb32aa9a1c44fb1b7b41d', 1, '2017-09-04 12:42:26', 25),
('17d54d139d4c719a7312e3925daf9cd6', 2, '2017-09-04 12:43:32', 26),
('e4357031e8494fa9f4412dd375718aa1', 1, '2017-09-04 13:22:44', 27),
('c03f189550177f6dd7e30c40b573bd4a', 2, '2017-09-04 13:24:39', 28),
('74986139a87740eb173cf8bd07415cff', 1, '2017-09-04 13:31:28', 29),
('9c0cdf2e889503cd5c71a71cfcadbab9', 1, '2017-09-04 13:32:10', 30),
('7c39865d74503b1462f109f46bd48373', 2, '2017-09-04 13:33:26', 31),
('b4a8c30e62552138383cd155e3e8729e', 2, '2017-09-04 13:33:32', 32),
('9c44333a6892d951402753998b8cbe50', 2, '2017-09-04 17:05:32', 33),
('19240f9501a4b9ed89a610512d418973', 2, '2017-09-04 17:14:44', 34),
('e083755d762d675461e76ef08381e6a0', 1, '2017-09-04 22:44:16', 35),
('0de1d52748c9d4b3262b43f92a12d2e1', 2, '2017-09-04 23:34:27', 36),
('ed72c00b8a966da45bcea3833b944c76', 2, '2017-09-04 23:37:10', 37),
('484627111258f51dfde43650b259fed2', 2, '2017-09-04 23:38:41', 38),
('7eb4c509376d359d216794ac6966e95c', 1, '2017-09-04 23:39:10', 39),
('4a28523934bc40139c1d4f2c6a9246f7', 2, '2017-09-04 23:41:26', 40),
('94eaf0503b19d2363d3ee50330c93e11', 1, '2017-09-05 00:08:28', 41),
('08851eb9d58ba573ba5bcb31c955da6b', 2, '2017-09-05 00:19:52', 42),
('01681459c47af060349487a5dc709afe', 4, '2017-09-05 13:42:39', 43),
('1c7729a3e0faaa264fe6ee6dc7df362d', 1, '2017-09-05 13:47:35', 44),
('838872598a816af3b165f8216be1214b', 5, '2017-09-06 07:57:31', 45),
('0a501a0512399e879507ac95263d34ac', 6, '2017-09-10 10:43:04', 46),
('43d3e5d47d28179d1d2562e477ec7fa7', 6, '2017-09-13 06:11:57', 47),
('3be2eec7c3eafe0132a9c6e9fa2cd5a7', 8, '2017-09-13 06:47:47', 48),
('14f3bb7df44e0681ff9da9f55fc1ba99', 6, '2017-09-13 07:16:44', 49),
('bfc3c294d040161f00c7773091c8d5e1', 6, '2017-09-13 07:17:11', 50),
('ff4867050ab3873da3795e7ac9ba116f', 8, '2017-09-13 07:42:42', 51),
('77d18c51370ccb1dc76044b0da3ee9ce', 8, '2017-09-13 09:17:10', 52),
('19a7fae32a80d444f61f22182b3723da', 1, '2017-09-13 20:08:37', 53),
('2b1c5a2660520fb84590cce3abcc0f4f', 4, '2017-09-15 20:18:10', 54),
('b5d196a1520955b873ce60cd93cf6f01', 1, '2017-09-16 23:45:11', 55),
('61ccf205b7c029657106890ca4abc68a', 2, '2017-09-17 01:17:18', 56),
('42e4a5b0594464669b804390f18af162', 1, '2017-09-17 01:23:20', 57),
('7abee09b5ba6424bdf4938f2f27d5b0d', 2, '2017-09-17 01:24:16', 58),
('d1bdf0b9175ea0858cdfa0631e55a5f3', 2, '2017-09-17 01:43:43', 59),
('6d69947aae73936ae472e4b799c9450f', 2, '2017-09-17 06:07:40', 60),
('843c6b5396b658e86b76fe14d164de12', 2, '2017-09-17 17:15:02', 61),
('89bac98f36871fb4dab3cc101a701727', 2, '2017-09-17 18:50:37', 62),
('0566fa708d669e3e5d4900d5c409e2b3', 2, '2017-09-17 18:56:14', 63),
('2b61920c80f0933d539975205dbb45cd', 1, '2017-09-18 11:28:02', 64),
('0dde8629afa3564134896d5198883567', 4, '2017-09-18 12:33:51', 65),
('b1581fbfc7d885b0b548469d291915e0', 2, '2017-09-18 14:10:35', 66),
('51aa6c6eed02bca51a8a5a8c01c91500', 1, '2017-09-18 14:11:01', 67),
('9ac3e3ae755acff15621c4e448ed9bc5', 2, '2017-09-18 15:05:54', 68),
('1dfb3c8453ad2a00b6c6330bb0eb5e16', 1, '2017-09-18 15:33:49', 69),
('9d74a0173c627a59db5c2eebfdfa5fbb', 2, '2017-09-21 06:41:39', 70),
('27e3c0c87cbb10fb32ed67a641c34c6e', 1, '2017-09-21 09:06:46', 71),
('4b989ee57a2d0546d436de9ca6f7f6a2', 2, '2017-09-21 09:08:58', 72),
('3f314a670a66a4d020f3735a0d9e6370', 8, '2017-09-21 17:14:57', 73),
('a30df6cde22fe647a8e4f36f7c494e99', 6, '2017-09-22 06:16:15', 74),
('c87d5e46e653ca2498d9ed2ed7f06d2f', 2, '2017-09-23 22:06:28', 75),
('f25cb01ce49dba855291ea3eeb7312b3', 1, '2017-09-27 12:49:08', 76),
('76a88e8085998f72d46da3e9026f04e9', 1, '2017-09-27 12:49:41', 77),
('0c53e6c237fa1a28656f9bc457c267cb', 6, '2017-09-27 18:31:46', 78),
('abffdbb4151331d176bcbb85c7388077', 1, '2017-10-09 20:36:15', 79);

-- --------------------------------------------------------

--
-- Struktur dari tabel `asam_urat`
--

CREATE TABLE `asam_urat` (
  `idAsam_Urat` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `nilai` decimal(10,2) DEFAULT NULL,
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `asam_urat`
--

INSERT INTO `asam_urat` (`idAsam_Urat`, `idUser`, `nilai`, `date`) VALUES
(1, 1, '8.50', '2017-09-22 06:18:36'),
(2, 3, '8.50', '2017-09-22 06:18:36'),
(3, 5, '8.50', '2017-09-22 06:18:36');

-- --------------------------------------------------------

--
-- Struktur dari tabel `authentication`
--

CREATE TABLE `authentication` (
  `idAuthentication` int(11) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `role` varchar(20) NOT NULL,
  `firebase_token` text,
  `status` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `authentication`
--

INSERT INTO `authentication` (`idAuthentication`, `username`, `password`, `email`, `role`, `firebase_token`, `status`) VALUES
(1, 'kincat', 'c4ca4238a0b923820dcc509a6f75849b', 'yohankinata@gmail.com', 'user', 'fnxYewIRmlU:APA91bES6CZkBQZYRteQbt3qon0j3MH3qY-xnGKumfu8CWY3wccM47fWMI92w_WDPNWhnamU0rsQqMu6FbubBGwfl9Q4gRg45nLZwQN3JmbNL8nJ-sCfyvt8fT3OFiHJLL_MDDZ3rFPL', 2),
(2, 'kincatvol', 'c4ca4238a0b923820dcc509a6f75849b', 'yohankinata@gmail.com', 'volunteer', 'fYf4zKuBR6k:APA91bHKwniobrSyZNcbg9gNO4jOOWv50a1Pw7bppFnIBufVNAZVcqELw6i4KVJKjulZNLsL0kPJDL8YYCU8iTCK62pEGJ5guYkhIeTuMnsnQg3o3HJOMIhjoyElE5RW5Sn2mmvWTNJ7', 2),
(3, 'Alwi', 'ca9227d13f26f24b4dbcb047a46fe4e9', 'alwislasher12@gmail.con', 'user', NULL, 0),
(4, 'Tri', 'c892ba238c98835d4d53a3faed43ee52', 'trimurti890@gmail.com', 'user', 'f-oJu9-dLy0:APA91bGECUq8os5rOFdeuQ6VsxOHFBzb7Gi3IDS09_2jkXKxO9-KKAkyLyHfD3pjjALNEjoyZE-3UQHebyTP6GaPse5mAuKQGC1_yyb33dpnqV5uEcnvnJJLfKt15663KtffUzbYgDkS', 2),
(5, 'storage', '827ccb0eea8a706c4c34a16891f84e7b', 'storagesaya1@gmail.com', 'user', 'f8mS5uQHu2o:APA91bHQH6M1OdvFG1YMV7toW74LVuTOC2kJQCLz8H6uvhJ9IROMTOi9yXT1txr3Nv-QzJdpFrspm6GP2S6Ft_28_GoSvrJJ-RZfzOhAWCX4TEbEgLCLhR4iB8cG_ld1tsCgSsVDvJ9m', 2),
(6, 'Sigit2404', '3bd74f35fba5f9ada55d0ad44fb242aa', 'igitpg@gmail.com', 'user', 'dP0tZTowQe8:APA91bGc-3VcHQU88H2OEweYeIdIZOR3XFKz2FtSdB0ZiVOa9sXW8kuXOlCVou-ODdAncHLSr_LmvbuxUt3e2E0z7SR7VdVu1D1HxOgVGd8HysGdMjY_Kkby-Y0I4plns7Z2lQQeVkCc', 2),
(7, 'kincat1', 'c4ca4238a0b923820dcc509a6f75849b', 'yohankinata@gmail.com', 'user', NULL, 0),
(8, 'Sigitmn', '25588e83ddf2137fd868abfc95c33353', 'sigitmnuzul@gmail.com', 'volunteer', 'frJgjhHd9q0:APA91bFgLIBsnHbN4lUkNE-yfHM-3PDC6AYoLfcvXT7bsfHdnWT3oK8Nko_WlR0yTYTH5XY14S2vUgg6nmRfXgm1NWwF5yN0Pj0NmiOXmgWxuNU-PQcZufuwI0FuXz408rAUIFWIfGq5', 2),
(9, 'testing', 'ae2b1fca515949e5d54fb22b8ed95575', 'testing@email.com', 'user', NULL, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `category`
--

CREATE TABLE `category` (
  `idCategory` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `category`
--

INSERT INTO `category` (`idCategory`, `name`, `description`) VALUES
(1, 'penyakit', 'kumpulan penyakit'),
(2, 'obat', 'kumpulan obat'),
(3, 'gaya hidup', 'kumpulan lifestyle'),
(4, 'herbal', 'kumpulan herbal');

-- --------------------------------------------------------

--
-- Struktur dari tabel `class_ambulance`
--

CREATE TABLE `class_ambulance` (
  `idClass` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `description` text,
  `type` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `class_ambulance`
--

INSERT INTO `class_ambulance` (`idClass`, `name`, `price`, `description`, `type`) VALUES
(1, 'Standart', '50000.00', 'Lorem Ipsum adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting.', 0),
(2, 'VIP', '100000.00', 'Lorem Ipsum adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting.', 0),
(3, 'VVIP', '200000.00', 'Lorem Ipsum adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting.', 0),
(4, 'Standart', '100000.00', 'Lorem Ipsum adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting.', 1),
(5, 'VIP', '200000.00', 'Lorem Ipsum adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting.', 1),
(6, 'VVIP', '500000.00', 'Lorem Ipsum adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting.', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `document`
--

CREATE TABLE `document` (
  `idDocument` int(11) NOT NULL,
  `idVolunteer` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` text,
  `path` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `document`
--

INSERT INTO `document` (`idDocument`, `idVolunteer`, `name`, `description`, `path`) VALUES
(1, 2, 'u', 'j', '2_u.png'),
(2, 3, 'pelatihan bhd', 'pelatihan bantuan hidup dasar dgn hipgabi', '8_pelatihanbhd.png'),
(3, 3, 'id card', 'id card hipgabi', '8_idcard.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gula_darah`
--

CREATE TABLE `gula_darah` (
  `idGula_Darah` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `nilai` decimal(10,2) DEFAULT NULL,
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `gula_darah`
--

INSERT INTO `gula_darah` (`idGula_Darah`, `idUser`, `nilai`, `date`) VALUES
(1, 1, '58.00', '2017-09-03 11:18:36'),
(2, 3, '737346.00', '2017-09-05 13:46:40'),
(3, 5, '123.00', '2017-09-13 06:15:05');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hemoglobin`
--

CREATE TABLE `hemoglobin` (
  `idHemoglobin` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `nilai` decimal(10,2) DEFAULT NULL,
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `hemoglobin`
--

INSERT INTO `hemoglobin` (`idHemoglobin`, `idUser`, `nilai`, `date`) VALUES
(1, 1, '8.50', '2017-09-17 00:35:29'),
(2, 5, '8.50', '2017-09-17 00:35:29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kolesterol`
--

CREATE TABLE `kolesterol` (
  `idKolesterol` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `nilai` decimal(10,2) DEFAULT NULL,
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `kolesterol`
--

INSERT INTO `kolesterol` (`idKolesterol`, `idUser`, `nilai`, `date`) VALUES
(1, 1, '120.00', '2017-09-13 06:15:36'),
(2, 5, '120.00', '2017-09-13 06:15:36');

-- --------------------------------------------------------

--
-- Struktur dari tabel `notification`
--

CREATE TABLE `notification` (
  `idnotification` int(11) NOT NULL,
  `createdDate` varchar(45) DEFAULT NULL,
  `createdTime` varchar(45) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `idAuth` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `order`
--

CREATE TABLE `order` (
  `idOrder` int(11) NOT NULL,
  `idAmbulance` int(11) DEFAULT NULL,
  `idUser` int(11) NOT NULL,
  `idClass` int(11) DEFAULT NULL,
  `createdTime` datetime DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `notes_user` text,
  `paymentStatus` varchar(45) DEFAULT NULL,
  `destination` text,
  `no_hp` varchar(25) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `notesAmbulanceVolunteer` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `order`
--

INSERT INTO `order` (`idOrder`, `idAmbulance`, `idUser`, `idClass`, `createdTime`, `status`, `notes_user`, `paymentStatus`, `destination`, `no_hp`, `longitude`, `latitude`, `notesAmbulanceVolunteer`) VALUES
(1, 1, 1, 2, '2017-09-04 23:13:21', '2', 'bbbb', 'Belum Lunas', 'Jl. Dahlia IV No.134, Depok Jaya, Pancoran MAS, Kota Depok, Jawa Barat 16432, Indonesia/nsss', '44', '106.8132031', '-6.3906898', NULL),
(2, 0, 1, NULL, '2017-09-04 23:40:30', '3', 'nnnn', 'Belum Lunas', 'Jl. Dahlia IV No.134, Depok Jaya, Pancoran MAS, Kota Depok, Jawa Barat 16432, Indonesia\nhhhjj', '888', '106.8132031', '-6.3906898', NULL),
(3, 1, 1, 2, '2017-09-05 00:19:20', '2', 'yjj', 'Belum Lunas', 'Jl. Murai Blok Ff No.11, Pd. Bambu, Duren Sawit, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta 13430, Indonesia\nkantor', '089653823548', '106.892054', '-6.2319016', NULL),
(4, 1, 3, 2, '2017-09-05 14:19:57', '1', NULL, 'Belum Lunas', 'Jl. Irian Jaya No.81, Depok Jaya, Pancoran MAS, Kota Depok, Jawa Barat 16432, Indonesia\ndjdjdj', '64644949464649', '106.8101177', '-6.3932458', NULL),
(5, 0, 4, NULL, '2017-09-06 07:59:18', '0', NULL, 'Belum Lunas', 'Jl. Prof. DR. Nugroho Notosutanto, Pd. Cina, Beji, Kota Depok, Jawa Barat 16424, Indonesia\nSip', '088', '106.8284467', '-6.3645883', NULL),
(6, 1, 5, 2, '2017-09-13 06:23:04', '1', NULL, 'Belum Lunas', 'Jl. Kapuk No.14, Pd. Cina, Beji, Kota Depok, Jawa Barat 16424, Indonesia\nsaya dlt. 2', '082348371262', '106.8379331', '-6.3689382', NULL),
(7, 1, 1, 2, '2017-09-13 20:09:22', '2', 'bbb', 'Belum Lunas', 'Jl. Dahlia IV No.134, Depok Jaya, Pancoran MAS, Kota Depok, Jawa Barat 16432, Indonesia\nuja', '44', '106.8132031', '-6.3906898', NULL),
(8, 1, 1, 2, '2017-09-16 14:04:15', '2', 'ok', 'Belum Lunas', 'J, Kukusan, Beji, Kota Depok, Jawa Barat 16425, Indonesia\nbhuu', '558', '106.8243942', '-6.3719272', NULL),
(9, 1, 1, 2, '2017-09-17 02:21:42', '2', 'selesai', 'Belum Lunas', 'Gg. Jempol, Puspasari, Citeureup, Bogor, Jawa Barat 16810, Indonesia\ndarurat nih', '089653823548', '106.8802635', '-6.4756442', NULL),
(10, 1, 1, 2, '2017-09-17 06:48:45', '1', NULL, 'Belum Lunas', 'Jalan Kampung Puspa Negara No.97, Puspanegara, Citeureup, Bogor, Jawa Barat 16810, Indonesia\ngg. jempol', '089653823548', '106.880221', '-6.4754459', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `order_detail`
--

CREATE TABLE `order_detail` (
  `idOrderDetail` int(11) NOT NULL,
  `idOrder` int(11) NOT NULL,
  `idVolunteer` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `datecreate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateupdate` datetime DEFAULT NULL,
  `notes_user` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `order_detail`
--

INSERT INTO `order_detail` (`idOrderDetail`, `idOrder`, `idVolunteer`, `status`, `datecreate`, `dateupdate`, `notes_user`) VALUES
(1, 1, 1, 0, '2017-09-04 16:13:25', NULL, NULL),
(2, 1, 2, 3, '2017-09-04 16:13:25', '2017-09-04 23:36:53', NULL),
(3, 2, 1, 2, '2017-09-04 16:40:37', '2017-09-04 23:41:00', 'User:nnnn'),
(4, 2, 2, 2, '2017-09-04 16:40:38', '2017-09-04 23:41:10', 'User:weqww'),
(5, 3, 1, 0, '2017-09-04 17:19:24', NULL, NULL),
(6, 3, 2, 0, '2017-09-04 17:19:25', NULL, NULL),
(7, 5, 1, 0, '2017-09-06 00:59:39', NULL, NULL),
(8, 5, 2, 0, '2017-09-06 00:59:40', NULL, NULL),
(9, 6, 1, 0, '2017-09-12 23:23:41', NULL, NULL),
(10, 6, 2, 3, '2017-09-12 23:23:42', '2017-09-18 15:25:37', NULL),
(11, 7, 1, 0, '2017-09-13 13:09:30', NULL, NULL),
(12, 7, 2, 3, '2017-09-13 13:09:31', '2017-09-18 15:24:03', NULL),
(13, 7, 3, 0, '2017-09-13 13:09:31', NULL, NULL),
(14, 8, 1, 0, '2017-09-16 07:04:18', NULL, NULL),
(15, 8, 2, 0, '2017-09-16 07:04:19', NULL, NULL),
(16, 8, 3, 0, '2017-09-16 07:04:19', NULL, NULL),
(17, 9, 1, 0, '2017-09-16 19:21:47', NULL, NULL),
(18, 9, 2, 1, '2017-09-16 19:21:47', '2017-09-21 06:42:39', NULL),
(19, 9, 3, 1, '2017-09-16 19:21:48', '2017-09-21 17:45:09', NULL),
(20, 10, 1, 0, '2017-09-16 23:48:49', NULL, NULL),
(21, 10, 2, 2, '2017-09-16 23:48:50', '2017-09-17 20:48:50', 'Volunteer:males'),
(22, 10, 3, 3, '2017-09-16 23:48:50', '2017-09-21 17:36:00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `post`
--

CREATE TABLE `post` (
  `idPost` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `postDate` datetime DEFAULT NULL,
  `content` text,
  `createdTime` datetime DEFAULT NULL,
  `createdBy` varchar(100) DEFAULT NULL,
  `updateTime` datetime DEFAULT NULL,
  `updateBy` varchar(100) DEFAULT NULL,
  `author` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `post`
--

INSERT INTO `post` (`idPost`, `title`, `thumbnail`, `postDate`, `content`, `createdTime`, `createdBy`, `updateTime`, `updateBy`, `author`) VALUES
(1, 'herbal for life', 'default.png', '2017-07-27 00:00:00', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum mollis at eros at accumsan. Nulla feugiat maximus nibh, eget commodo massa posuere sit amet. Morbi id magna a nibh tincidunt suscipit. Praesent nec justo tempus, laoreet quam a, pulvinar leo. Maecenas aliquet lectus tincidunt nibh volutpat malesuada. Aenean pretium arcu a augue hendrerit pulvinar. Vivamus dapibus elementum accumsan. Etiam finibus est ante, a finibus dui rutrum sit amet. Donec in risus ipsum. Sed non eros augue. Proin sit amet suscipit mi. Ut ac dignissim sem, ac viverra nisl.', '2017-07-28 00:00:00', 'kincat', NULL, NULL, 'kincat'),
(2, 'ayan', 'default.png', '2017-07-27 00:00:00', 'Nam efficitur in lectus ut facilisis. Vestibulum sollicitudin interdum quam, id tincidunt ipsum malesuada a. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc et odio lectus. Fusce congue leo id consectetur convallis. Donec lacinia ligula sit amet gravida consequat. Aenean nec mi sodales est ornare imperdiet pulvinar in ex. Maecenas blandit rhoncus tortor, sit amet finibus est maximus at. Morbi mollis vestibulum molestie.', '2017-07-27 00:00:00', 'kucing', '2017-07-13 00:00:00', 'kucing', 'kucing');

-- --------------------------------------------------------

--
-- Struktur dari tabel `post_to_category`
--

CREATE TABLE `post_to_category` (
  `idCategory` int(11) NOT NULL,
  `idPost` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `post_to_category`
--

INSERT INTO `post_to_category` (`idCategory`, `idPost`) VALUES
(1, 2),
(2, 1),
(4, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tekanan_darah`
--

CREATE TABLE `tekanan_darah` (
  `idTekanan_Darah` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `sistol` decimal(10,2) DEFAULT NULL,
  `diastol` decimal(10,2) DEFAULT NULL,
  `nadi` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tekanan_darah`
--

INSERT INTO `tekanan_darah` (`idTekanan_Darah`, `idUser`, `sistol`, `diastol`, `nadi`) VALUES
(1, 1, '58.00', '88.00', '8.00'),
(2, 3, '58.00', '88.00', '8.00'),
(3, 5, '120.00', '80.00', '98.00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `iduser` int(11) NOT NULL,
  `idAuth` int(11) NOT NULL,
  `displayName` varchar(45) DEFAULT NULL,
  `dateofBirth` date DEFAULT NULL,
  `golonganDarah` varchar(45) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `tinggiBadan` decimal(10,2) DEFAULT NULL,
  `riwayatKesehatan` text,
  `handphone` varchar(45) DEFAULT NULL,
  `photo` varchar(100) DEFAULT 'default.png',
  `biografi` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`iduser`, `idAuth`, `displayName`, `dateofBirth`, `golonganDarah`, `address`, `tinggiBadan`, `riwayatKesehatan`, `handphone`, `photo`, `biografi`) VALUES
(1, 1, 'lkincat oke', '2017-09-23', 'AB', 'hh', '5555.00', 'hhh', '089653823548', 'default.png', 'Biografinya apa aja deh'),
(2, 3, 'Alwi', NULL, NULL, NULL, NULL, NULL, NULL, 'default.png', NULL),
(3, 4, 'Tri', '1967-09-01', 'A', 'jalan', '175.00', 'Batuk', '12345678910', 'default.png', 'sakit'),
(4, 5, 'storage', '2017-09-04', 'O', 'Kapuk', '172.00', 'ok', '088', '5.png', 'Biografi'),
(5, 6, 'Sigit', '1989-04-24', 'B', 'Jl. M Tohir', '178.00', 'magh', '082348371262', '6.png', 'mahasiswa'),
(6, 7, 'kincat1', NULL, NULL, NULL, NULL, NULL, NULL, 'default.png', NULL),
(7, 9, 'testing', NULL, NULL, NULL, NULL, NULL, NULL, 'default.png', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `volunteer`
--

CREATE TABLE `volunteer` (
  `idVolunteer` int(11) NOT NULL,
  `idAuth` int(11) NOT NULL,
  `displayName` varchar(45) DEFAULT NULL,
  `dateofBirth` date DEFAULT NULL,
  `golonganDarah` varchar(45) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `tinggiBadan` decimal(10,2) DEFAULT NULL,
  `riwayatKesehatan` text,
  `handphone` varchar(45) DEFAULT NULL,
  `photo` varchar(100) DEFAULT 'default.png',
  `biografi` text,
  `status` int(1) DEFAULT '0',
  `latitude` varchar(100) DEFAULT NULL,
  `longitude` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `volunteer`
--

INSERT INTO `volunteer` (`idVolunteer`, `idAuth`, `displayName`, `dateofBirth`, `golonganDarah`, `address`, `tinggiBadan`, `riwayatKesehatan`, `handphone`, `photo`, `biografi`, `status`, `latitude`, `longitude`) VALUES
(1, 1, 'Kincat_volunteer', '2017-09-01', 'AB', NULL, '123.00', 'asdasd', '123123123', 'default.png', 'ajsdjasdj', 1, '30', '50'),
(2, 2, 'kincat volunteer', '2017-09-16', 'O', 'Jl. Taruna Bhakti, Curug, Cimanggis, Kota Depok, Jawa Barat 16453, Indonesia', '165.00', 'uu', '089653823548', '2.png', 'Biografinya apa aja deh', 1, '-6.2318313', '106.8920865'),
(3, 8, 'Sigitmn', '1989-04-24', 'B', 'Jl. Lb. Bulus I No.1A, Cilandak Bar., Cilandak, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12430, Indonesia', '178.00', 'BHD', '082348371262', '8.png', 'saya seorang perawat', 0, '-6.3655097', '106.8295778');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ambulance`
--
ALTER TABLE `ambulance`
  ADD PRIMARY KEY (`idAmbulance`);

--
-- Indexes for table `api_token`
--
ALTER TABLE `api_token`
  ADD PRIMARY KEY (`idToken`),
  ADD KEY `auth_API_idx` (`idAuth`);

--
-- Indexes for table `asam_urat`
--
ALTER TABLE `asam_urat`
  ADD PRIMARY KEY (`idAsam_Urat`),
  ADD KEY `au_to_user_idx` (`idUser`);

--
-- Indexes for table `authentication`
--
ALTER TABLE `authentication`
  ADD PRIMARY KEY (`idAuthentication`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`idCategory`);

--
-- Indexes for table `class_ambulance`
--
ALTER TABLE `class_ambulance`
  ADD PRIMARY KEY (`idClass`);

--
-- Indexes for table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`idDocument`),
  ADD KEY `doc_to_volunteer_idx` (`idVolunteer`);

--
-- Indexes for table `gula_darah`
--
ALTER TABLE `gula_darah`
  ADD PRIMARY KEY (`idGula_Darah`),
  ADD KEY `gd_to_user_idx` (`idUser`);

--
-- Indexes for table `hemoglobin`
--
ALTER TABLE `hemoglobin`
  ADD PRIMARY KEY (`idHemoglobin`),
  ADD KEY `hg_to_user_idx` (`idUser`);

--
-- Indexes for table `kolesterol`
--
ALTER TABLE `kolesterol`
  ADD PRIMARY KEY (`idKolesterol`),
  ADD KEY `kl_to_user_idx` (`idUser`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`idnotification`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`idOrder`),
  ADD KEY `ambulance_idx` (`idAmbulance`),
  ADD KEY `order_to_user_idx` (`idUser`);

--
-- Indexes for table `order_detail`
--
ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`idOrderDetail`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`idPost`);

--
-- Indexes for table `post_to_category`
--
ALTER TABLE `post_to_category`
  ADD PRIMARY KEY (`idCategory`,`idPost`),
  ADD KEY `categorytopost_idx` (`idPost`);

--
-- Indexes for table `tekanan_darah`
--
ALTER TABLE `tekanan_darah`
  ADD PRIMARY KEY (`idTekanan_Darah`),
  ADD KEY `td_to_user_idx` (`idUser`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`iduser`),
  ADD KEY `auth_idx` (`idAuth`);

--
-- Indexes for table `volunteer`
--
ALTER TABLE `volunteer`
  ADD PRIMARY KEY (`idVolunteer`),
  ADD KEY `auth_idx` (`idAuth`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ambulance`
--
ALTER TABLE `ambulance`
  MODIFY `idAmbulance` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `api_token`
--
ALTER TABLE `api_token`
  MODIFY `idToken` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;
--
-- AUTO_INCREMENT for table `asam_urat`
--
ALTER TABLE `asam_urat`
  MODIFY `idAsam_Urat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `authentication`
--
ALTER TABLE `authentication`
  MODIFY `idAuthentication` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `idCategory` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `class_ambulance`
--
ALTER TABLE `class_ambulance`
  MODIFY `idClass` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `document`
--
ALTER TABLE `document`
  MODIFY `idDocument` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `gula_darah`
--
ALTER TABLE `gula_darah`
  MODIFY `idGula_Darah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `hemoglobin`
--
ALTER TABLE `hemoglobin`
  MODIFY `idHemoglobin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `kolesterol`
--
ALTER TABLE `kolesterol`
  MODIFY `idKolesterol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `idnotification` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `idOrder` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `order_detail`
--
ALTER TABLE `order_detail`
  MODIFY `idOrderDetail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `idPost` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tekanan_darah`
--
ALTER TABLE `tekanan_darah`
  MODIFY `idTekanan_Darah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `volunteer`
--
ALTER TABLE `volunteer`
  MODIFY `idVolunteer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `api_token`
--
ALTER TABLE `api_token`
  ADD CONSTRAINT `api_to_auth` FOREIGN KEY (`idAuth`) REFERENCES `authentication` (`idAuthentication`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `asam_urat`
--
ALTER TABLE `asam_urat`
  ADD CONSTRAINT `au_to_user` FOREIGN KEY (`idUser`) REFERENCES `user` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `document`
--
ALTER TABLE `document`
  ADD CONSTRAINT `doc_to_volunteer` FOREIGN KEY (`idVolunteer`) REFERENCES `volunteer` (`idVolunteer`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `gula_darah`
--
ALTER TABLE `gula_darah`
  ADD CONSTRAINT `gd_to_user` FOREIGN KEY (`idUser`) REFERENCES `user` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `hemoglobin`
--
ALTER TABLE `hemoglobin`
  ADD CONSTRAINT `hg_to_user` FOREIGN KEY (`idUser`) REFERENCES `user` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `kolesterol`
--
ALTER TABLE `kolesterol`
  ADD CONSTRAINT `kl_to_user` FOREIGN KEY (`idUser`) REFERENCES `user` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `order_to_user` FOREIGN KEY (`idUser`) REFERENCES `user` (`iduser`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `post_to_category`
--
ALTER TABLE `post_to_category`
  ADD CONSTRAINT `categorytocategory` FOREIGN KEY (`idCategory`) REFERENCES `category` (`idCategory`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `categorytopost` FOREIGN KEY (`idPost`) REFERENCES `post` (`idPost`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tekanan_darah`
--
ALTER TABLE `tekanan_darah`
  ADD CONSTRAINT `td_to_user` FOREIGN KEY (`idUser`) REFERENCES `user` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_to_auth` FOREIGN KEY (`idAuth`) REFERENCES `authentication` (`idAuthentication`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `volunteer`
--
ALTER TABLE `volunteer`
  ADD CONSTRAINT `volunteer_to_auth` FOREIGN KEY (`idAuth`) REFERENCES `authentication` (`idAuthentication`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
