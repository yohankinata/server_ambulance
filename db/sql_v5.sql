-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 28 Jul 2017 pada 11.53
-- Versi Server: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ambulance`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `ambulance`
--

CREATE TABLE `ambulance` (
  `idAmbulance` int(11) NOT NULL,
  `idAuth` int(11) NOT NULL,
  `longitudeGmaps` varchar(255) DEFAULT NULL,
  `latitudeGmaps` varchar(255) DEFAULT NULL,
  `handphone` varchar(45) DEFAULT NULL,
  `nomorPolisi` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `type` varchar(50) NOT NULL,
  `class` varchar(50) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `description` text NOT NULL,
  `notes` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ambulance`
--

INSERT INTO `ambulance` (`idAmbulance`, `idAuth`, `longitudeGmaps`, `latitudeGmaps`, `handphone`, `nomorPolisi`, `status`, `type`, `class`, `price`, `description`, `notes`) VALUES
(1, 13, '106.82899475', '-6.44358853', '081293123', 'B1239', '1', 'standart', 'vip', '7000000.00', 'awaakwkawk', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `api_token`
--

CREATE TABLE `api_token` (
  `API_Token` text NOT NULL,
  `idAuth` int(11) NOT NULL,
  `createdDate` datetime DEFAULT NULL,
  `idToken` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `api_token`
--

INSERT INTO `api_token` (`API_Token`, `idAuth`, `createdDate`, `idToken`) VALUES
('dfaff1e341c0bd0f4e34346aa0f595e0', 14, '2017-07-26 15:58:34', 1),
('296b1cae0f29175a10e07d0df2fd3b58', 14, '2017-07-26 17:51:11', 2),
('daa0eab004fbc70817d7faa9eb78d0c2', 13, '2017-07-26 17:55:38', 3),
('025fe699a29040bcb0882825e59f08c3', 14, '2017-07-27 12:50:17', 4),
('0e3a73839aa83dd99c084d1b661229e6', 13, '2017-07-28 11:37:13', 5),
('e7ff8b609d9e13efdd51ec1660aa0b3f', 14, '2017-07-28 16:43:49', 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `asam_urat`
--

CREATE TABLE `asam_urat` (
  `idAsam_Urat` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `nilai` decimal(10,2) DEFAULT NULL,
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `asam_urat`
--

INSERT INTO `asam_urat` (`idAsam_Urat`, `idUser`, `nilai`, `date`) VALUES
(1, 5, '125.00', '2017-07-27 11:32:33');

-- --------------------------------------------------------

--
-- Struktur dari tabel `authentication`
--

CREATE TABLE `authentication` (
  `idAuthentication` int(11) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `role` varchar(20) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `authentication`
--

INSERT INTO `authentication` (`idAuthentication`, `username`, `password`, `email`, `role`, `status`) VALUES
(13, 'kincat', 'c81e728d9d4c2f636f067f89cc14862c', 'yohankinata@gmail.com', 'user', 1),
(14, 'kadal', 'c4ca4238a0b923820dcc509a6f75849b', 'yohankinata@gmail.com', 'volunteer', 1),
(15, 'ambulance1', 'c4ca4238a0b923820dcc509a6f75849b', 'yohankinata@gmail.com', 'ambulance', 1),
(16, 'ambulance1', 'c4ca4238a0b923820dcc509a6f75849b', 'yohankinata@gmail.com', 'ambulance', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `category`
--

CREATE TABLE `category` (
  `idCategory` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `category`
--

INSERT INTO `category` (`idCategory`, `name`, `description`) VALUES
(1, 'penyakit', 'kumpulan penyakit'),
(2, 'obat', 'kumpulan obat'),
(3, 'gaya hidup', 'kumpulan lifestyle'),
(4, 'herbal', 'kumpulan herbal');

-- --------------------------------------------------------

--
-- Struktur dari tabel `document`
--

CREATE TABLE `document` (
  `idDocument` int(11) NOT NULL,
  `idVolunteer` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` text,
  `path` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `document`
--

INSERT INTO `document` (`idDocument`, `idVolunteer`, `name`, `description`, `path`) VALUES
(5, 4, 'sertifikat p3', 'gratisan', 'beach.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gula_darah`
--

CREATE TABLE `gula_darah` (
  `idGula_Darah` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `nilai` decimal(10,2) DEFAULT NULL,
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `gula_darah`
--

INSERT INTO `gula_darah` (`idGula_Darah`, `idUser`, `nilai`, `date`) VALUES
(1, 5, '500.00', '2017-07-27 11:32:33');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hemoglobin`
--

CREATE TABLE `hemoglobin` (
  `idHemoglobin` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `nilai` decimal(10,2) DEFAULT NULL,
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kolesterol`
--

CREATE TABLE `kolesterol` (
  `idKolesterol` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `nilai` decimal(10,2) DEFAULT NULL,
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `notification`
--

CREATE TABLE `notification` (
  `idnotification` int(11) NOT NULL,
  `createdDate` varchar(45) DEFAULT NULL,
  `createdTime` varchar(45) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `idAuth` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `order`
--

CREATE TABLE `order` (
  `idOrder` int(11) NOT NULL,
  `idAmbulance` int(11) DEFAULT NULL,
  `idUser` int(11) NOT NULL,
  `createdTime` datetime DEFAULT NULL,
  `type` varchar(20) NOT NULL,
  `class` varchar(50) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `notes_user` text,
  `price` decimal(10,2) DEFAULT NULL,
  `paymentStatus` varchar(45) DEFAULT NULL,
  `destination` text,
  `longitude` varchar(255) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `notesAmbulanceVolunteer` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `order`
--

INSERT INTO `order` (`idOrder`, `idAmbulance`, `idUser`, `createdTime`, `type`, `class`, `status`, `notes_user`, `price`, `paymentStatus`, `destination`, `longitude`, `latitude`, `notesAmbulanceVolunteer`) VALUES
(1, NULL, 5, '2017-07-13 00:00:00', 'urgent', NULL, '0', NULL, '5000.00', NULL, NULL, '-6.4049516', '106.8172761', NULL),
(4, 1, 5, '2017-07-28 11:45:31', 'standart', 'normal', '0', NULL, NULL, NULL, 'depok', '106.82899475', '-6.44358853', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `order_detail`
--

CREATE TABLE `order_detail` (
  `idOrderDetail` int(11) NOT NULL,
  `idOrder` int(11) NOT NULL,
  `idVolunteer` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `datecreate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateupdate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `order_detail`
--

INSERT INTO `order_detail` (`idOrderDetail`, `idOrder`, `idVolunteer`, `status`, `datecreate`, `dateupdate`) VALUES
(13, 4, 4, 1, '2017-07-28 06:56:49', NULL),
(14, 4, 5, 0, '2017-07-28 06:57:03', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `post`
--

CREATE TABLE `post` (
  `idPost` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `postDate` datetime DEFAULT NULL,
  `content` text,
  `createdTime` datetime DEFAULT NULL,
  `createdBy` varchar(100) DEFAULT NULL,
  `updateTime` datetime DEFAULT NULL,
  `updateBy` varchar(100) DEFAULT NULL,
  `author` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `post`
--

INSERT INTO `post` (`idPost`, `title`, `thumbnail`, `postDate`, `content`, `createdTime`, `createdBy`, `updateTime`, `updateBy`, `author`) VALUES
(1, 'herbal for life', 'default.png', '2017-07-27 00:00:00', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum mollis at eros at accumsan. Nulla feugiat maximus nibh, eget commodo massa posuere sit amet. Morbi id magna a nibh tincidunt suscipit. Praesent nec justo tempus, laoreet quam a, pulvinar leo. Maecenas aliquet lectus tincidunt nibh volutpat malesuada. Aenean pretium arcu a augue hendrerit pulvinar. Vivamus dapibus elementum accumsan. Etiam finibus est ante, a finibus dui rutrum sit amet. Donec in risus ipsum. Sed non eros augue. Proin sit amet suscipit mi. Ut ac dignissim sem, ac viverra nisl.', '2017-07-28 00:00:00', 'kincat', NULL, NULL, 'kincat'),
(2, 'ayan', 'default.png', '2017-07-27 00:00:00', 'Nam efficitur in lectus ut facilisis. Vestibulum sollicitudin interdum quam, id tincidunt ipsum malesuada a. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc et odio lectus. Fusce congue leo id consectetur convallis. Donec lacinia ligula sit amet gravida consequat. Aenean nec mi sodales est ornare imperdiet pulvinar in ex. Maecenas blandit rhoncus tortor, sit amet finibus est maximus at. Morbi mollis vestibulum molestie.', '2017-07-27 00:00:00', 'kucing', '2017-07-13 00:00:00', 'kucing', 'kucing');

-- --------------------------------------------------------

--
-- Struktur dari tabel `post_to_category`
--

CREATE TABLE `post_to_category` (
  `idCategory` int(11) NOT NULL,
  `idPost` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `post_to_category`
--

INSERT INTO `post_to_category` (`idCategory`, `idPost`) VALUES
(1, 2),
(2, 1),
(4, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tekanan_darah`
--

CREATE TABLE `tekanan_darah` (
  `idTekanan_Darah` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `sistol` decimal(10,2) DEFAULT NULL,
  `diastol` decimal(10,2) DEFAULT NULL,
  `nadi` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `iduser` int(11) NOT NULL,
  `idAuth` int(11) NOT NULL,
  `displayName` varchar(45) DEFAULT NULL,
  `dateofBirth` date DEFAULT NULL,
  `golonganDarah` varchar(45) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `tinggiBadan` decimal(10,2) DEFAULT NULL,
  `riwayatKesehatan` text,
  `handphone` varchar(45) DEFAULT NULL,
  `photo` varchar(100) DEFAULT 'default.png'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`iduser`, `idAuth`, `displayName`, `dateofBirth`, `golonganDarah`, `address`, `tinggiBadan`, `riwayatKesehatan`, `handphone`, `photo`) VALUES
(5, 13, 'kincat_wokey', '2017-07-26', 'AB+', 'Depok', '175.00', 'Bengek', '11111111', '13.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `volunteer`
--

CREATE TABLE `volunteer` (
  `idVolunteer` int(11) NOT NULL,
  `idAuth` int(11) NOT NULL,
  `displayName` varchar(45) DEFAULT NULL,
  `dateofBirth` date DEFAULT NULL,
  `golonganDarah` varchar(45) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `tinggiBadan` decimal(10,2) DEFAULT NULL,
  `riwayatPenyakit` text,
  `handphone` varchar(45) DEFAULT NULL,
  `photo` varchar(100) DEFAULT 'default.png',
  `status` int(1) DEFAULT '0',
  `latitude` varchar(100) DEFAULT NULL,
  `longitude` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `volunteer`
--

INSERT INTO `volunteer` (`idVolunteer`, `idAuth`, `displayName`, `dateofBirth`, `golonganDarah`, `address`, `tinggiBadan`, `riwayatPenyakit`, `handphone`, `photo`, `status`, `latitude`, `longitude`) VALUES
(4, 14, 'kincat_wokey', '2017-07-26', 'AB+', 'Depok', '175.00', 'Bengek', '11111111', '14.png', 1, '-6.4147602', '106.76979303'),
(5, 15, 'kincat10', '2017-07-11', NULL, NULL, NULL, NULL, NULL, 'default.png', 1, '-6.43574192', '106.82882309'),
(6, 16, 'kincat11', NULL, NULL, NULL, NULL, NULL, NULL, 'default.png', 1, '-6.40128368', '106.77183151'),
(7, 13, 'kincat13', NULL, NULL, NULL, NULL, NULL, NULL, 'default.png', 1, '-6.38047114', '106.81097031');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ambulance`
--
ALTER TABLE `ambulance`
  ADD PRIMARY KEY (`idAmbulance`),
  ADD KEY `auth_idx` (`idAuth`);

--
-- Indexes for table `api_token`
--
ALTER TABLE `api_token`
  ADD PRIMARY KEY (`idToken`),
  ADD KEY `auth_API_idx` (`idAuth`);

--
-- Indexes for table `asam_urat`
--
ALTER TABLE `asam_urat`
  ADD PRIMARY KEY (`idAsam_Urat`),
  ADD KEY `au_to_user_idx` (`idUser`);

--
-- Indexes for table `authentication`
--
ALTER TABLE `authentication`
  ADD PRIMARY KEY (`idAuthentication`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`idCategory`);

--
-- Indexes for table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`idDocument`),
  ADD KEY `doc_to_volunteer_idx` (`idVolunteer`);

--
-- Indexes for table `gula_darah`
--
ALTER TABLE `gula_darah`
  ADD PRIMARY KEY (`idGula_Darah`),
  ADD KEY `gd_to_user_idx` (`idUser`);

--
-- Indexes for table `hemoglobin`
--
ALTER TABLE `hemoglobin`
  ADD PRIMARY KEY (`idHemoglobin`),
  ADD KEY `hg_to_user_idx` (`idUser`);

--
-- Indexes for table `kolesterol`
--
ALTER TABLE `kolesterol`
  ADD PRIMARY KEY (`idKolesterol`),
  ADD KEY `kl_to_user_idx` (`idUser`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`idnotification`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`idOrder`),
  ADD KEY `ambulance_idx` (`idAmbulance`),
  ADD KEY `order_to_user_idx` (`idUser`);

--
-- Indexes for table `order_detail`
--
ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`idOrderDetail`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`idPost`);

--
-- Indexes for table `post_to_category`
--
ALTER TABLE `post_to_category`
  ADD PRIMARY KEY (`idCategory`,`idPost`),
  ADD KEY `categorytopost_idx` (`idPost`);

--
-- Indexes for table `tekanan_darah`
--
ALTER TABLE `tekanan_darah`
  ADD PRIMARY KEY (`idTekanan_Darah`),
  ADD KEY `td_to_user_idx` (`idUser`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`iduser`),
  ADD KEY `auth_idx` (`idAuth`);

--
-- Indexes for table `volunteer`
--
ALTER TABLE `volunteer`
  ADD PRIMARY KEY (`idVolunteer`),
  ADD KEY `auth_idx` (`idAuth`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ambulance`
--
ALTER TABLE `ambulance`
  MODIFY `idAmbulance` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `api_token`
--
ALTER TABLE `api_token`
  MODIFY `idToken` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `asam_urat`
--
ALTER TABLE `asam_urat`
  MODIFY `idAsam_Urat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `authentication`
--
ALTER TABLE `authentication`
  MODIFY `idAuthentication` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `idCategory` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `document`
--
ALTER TABLE `document`
  MODIFY `idDocument` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `gula_darah`
--
ALTER TABLE `gula_darah`
  MODIFY `idGula_Darah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `hemoglobin`
--
ALTER TABLE `hemoglobin`
  MODIFY `idHemoglobin` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kolesterol`
--
ALTER TABLE `kolesterol`
  MODIFY `idKolesterol` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `idnotification` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `idOrder` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `order_detail`
--
ALTER TABLE `order_detail`
  MODIFY `idOrderDetail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `idPost` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tekanan_darah`
--
ALTER TABLE `tekanan_darah`
  MODIFY `idTekanan_Darah` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `volunteer`
--
ALTER TABLE `volunteer`
  MODIFY `idVolunteer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `ambulance`
--
ALTER TABLE `ambulance`
  ADD CONSTRAINT `ambulance_to_auth` FOREIGN KEY (`idAuth`) REFERENCES `authentication` (`idAuthentication`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `api_token`
--
ALTER TABLE `api_token`
  ADD CONSTRAINT `api_to_auth` FOREIGN KEY (`idAuth`) REFERENCES `authentication` (`idAuthentication`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `asam_urat`
--
ALTER TABLE `asam_urat`
  ADD CONSTRAINT `au_to_user` FOREIGN KEY (`idUser`) REFERENCES `user` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `document`
--
ALTER TABLE `document`
  ADD CONSTRAINT `doc_to_volunteer` FOREIGN KEY (`idVolunteer`) REFERENCES `volunteer` (`idVolunteer`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `gula_darah`
--
ALTER TABLE `gula_darah`
  ADD CONSTRAINT `gd_to_user` FOREIGN KEY (`idUser`) REFERENCES `user` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `hemoglobin`
--
ALTER TABLE `hemoglobin`
  ADD CONSTRAINT `hg_to_user` FOREIGN KEY (`idUser`) REFERENCES `user` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `kolesterol`
--
ALTER TABLE `kolesterol`
  ADD CONSTRAINT `kl_to_user` FOREIGN KEY (`idUser`) REFERENCES `user` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `order_to_user` FOREIGN KEY (`idUser`) REFERENCES `user` (`iduser`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `post_to_category`
--
ALTER TABLE `post_to_category`
  ADD CONSTRAINT `categorytocategory` FOREIGN KEY (`idCategory`) REFERENCES `category` (`idCategory`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `categorytopost` FOREIGN KEY (`idPost`) REFERENCES `post` (`idPost`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tekanan_darah`
--
ALTER TABLE `tekanan_darah`
  ADD CONSTRAINT `td_to_user` FOREIGN KEY (`idUser`) REFERENCES `user` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_to_auth` FOREIGN KEY (`idAuth`) REFERENCES `authentication` (`idAuthentication`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `volunteer`
--
ALTER TABLE `volunteer`
  ADD CONSTRAINT `volunteer_to_auth` FOREIGN KEY (`idAuth`) REFERENCES `authentication` (`idAuthentication`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
