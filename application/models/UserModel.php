<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserModel extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    public function get_self_profile($id,$role){
        $this->db->where('idAuth',$id);
        if($role == 'user'){
            $this->db->join('authentication','authentication.idAuthentication = user.idAuth');
            $query = $this->db->get('user');
        }else if($role == 'volunteer'){
            $this->db->join('authentication','authentication.idAuthentication = volunteer.idAuth');
            $query = $this->db->get('volunteer');
        }else{
            $this->db->join('authentication','authentication.idAuthentication = ambulance.idAuth');
            $query = $this->db->get('ambulance');
        }

        if($query){
            return array('status' => "OK",'result' => $query->row());
        }else{
            return array('status' => "ERROR",'message'=>'Error get data');
        }
    }

    public function get_user_profile($id){
        $this->db->where('idAuthentication',$id);
        $query = $this->db->get('authentication');
        if($query){
            if($query->row()->role == 'user'){
                $this->db->join('authentication','authentication.idAuthentication = user.idAuth');
                $this->db->where('idAuth',$id);
                $detail = $this->db->get('user');
            }else if($query->row()->role == 'volunteer'){
                $this->db->select('authentication.username,authentication.email,authentication.status as status_profile,authentication.role,authentication.firebase_token,volunteer.*');
                $this->db->join('authentication','authentication.idAuthentication = volunteer.idAuth');
                $this->db->where('idAuth',$id);
                $detail = $this->db->get('volunteer');
            }else{
                $this->db->where('idAuth',$id);
                $detail = $this->db->get('ambulance');
            }

            if($detail){
                if($query->row()->role == 'volunteer'){
                    $document = $this->get_volunteer_document($detail->row()->idVolunteer);
                    return array('status' => "OK",'result' => $detail->row(),'total_document'=>$document['total_row'],'document'=>$document['result']);
                }else if($query->row()->role == 'user'){
                    $asam = $this->get_advance_info($detail->row()->iduser,'asam_urat');
                    $gula = $this->get_advance_info($detail->row()->iduser,'gula_darah');
                    $tekanan = $this->get_advance_info($detail->row()->iduser,'tekanan_darah');
                    $kolestrol = $this->get_advance_info($detail->row()->iduser,'kolesterol');
                    $hemoglobin = $this->get_advance_info($detail->row()->iduser,'hemoglobin');
                    
                    return array('status' => "OK",'result' => $detail->row(),'asam_urat'=>$asam,'gula_darah'=>$gula,'tekanan'=>$tekanan,'kolestrol'=>$kolestrol,'hemoglobin'=>$hemoglobin);
                }else{
                    return array('status' => "OK",'result' => $detail->row());
                }
            }else{
                return array('status' => "ERROR",'message'=>'Error get data');
            }
        }else{
            return array('status' => "ERROR",'message'=>'Data not found!');
        }
    }

    public function get_volunteer_document($id){
        $this->db->where('idVolunteer',$id);
        $query = $this->db->get('document');
        if($query){
            return array('status' => "OK",'total_row'=>$query->num_rows(),'result' => $query->result());
        }else{
            return array('status' => "ERROR",'message'=>'Error get data');
        }
    }

    private function get_advance_info($id,$type){
        $this->db->where('idUser',$id);
        $query = $this->db->get($type);
        if($query){
            return array('total_row'=>$query->num_rows(),'detail' => $query->row());
        }else{
            return array('status' => "ERROR",'message'=>'Error get data');
        }
    }   

    public function get_id_by_auth($id,$type){
        $this->db->where('idAuth',$id);
        $query = $this->db->get($type);
        if($query){
            if($type == 'user'){
                return $query->row()->iduser;
            }else{
                return $query->row()->idVolunteer;
            }
        }else{
            return array('status' => "ERROR",'message'=>'Error get data');
        }
    }

    public function get_single_document($id){
        $this->db->where('idDocument',$id);
        $query = $this->db->get('document');
        if($query){
            return $query->row()->path;
        }else{
            return array('status' => "ERROR",'message'=>'Error get data');
        }
    }

    public function update_profile($id,$role,$data_auth,$data_profile){
        $this->db->where('idAuthentication',$id);
        $query_auth = $this->db->update('authentication',$data_auth);

        if($query_auth){
            $this->db->where('idAuth',$id);
            if($role == 'user'){
                $query = $this->db->update('user',$data_profile);
            }else{
                $query = $this->db->update('volunteer',$data_profile);
            }

            if($query){
                return array('status' => "OK",'messsage' => 'Success update profile!');
            }else{
                return array('status' => "ERROR",'messsage' => 'Failed update profile! Try Again');
            }
        }else{
            return array('status' => "ERROR",'messsage' => 'Failed update account! Try Again');
        }
    }
    
    public function update_photo($id,$role,$data_profile){
        $this->db->where('idAuth',$id);
        if($role == 'user'){
            $query = $this->db->update('user',$data_profile);
        }else{
            $query = $this->db->update('volunteer',$data_profile);
        }

        if($query){
            return array('status' => "OK",'messsage' => 'Success update photo profile!');
        }else{
            return array('status' => "ERROR",'messsage' => 'Failed update photo profile! Try Again');
        }
    }

    public function add_document($data){
        $query = $this->db->insert('document',$data);

        if($query){
            return array('status' => "OK",'messsage' => 'Success add document!');
        }else{
            return array('status' => "ERROR",'messsage' => 'Failed add document! Try Again');
        }
    }

    public function update_detail($iduser,$type,$data){
        $this->db->where('idUser',$iduser);
        $check = $this->get_advance_info($iduser,$type);

        if($check['total_row']>0){
            $query = $this->db->update($type,$data); 
        }else{
            $data['idUser'] = $iduser;
            $query = $this->db->insert($type,$data);
        }
        
        if($query){
            return array('status' => "OK",'messsage' => 'Success update detail information!');
        }else{
            return array('status' => "ERROR",'messsage' => 'Failed update detail information! Try Again');
        }
    }

    public function delete_detail($iduser,$type){
        $this->db->where('idUser',$iduser);
        $query = $this->db->delete($type);

        if($query){
            return array('status' => "OK",'messsage' => 'Success delete detail information!');
        }else{
            return array('status' => "ERROR",'messsage' => 'Failed delete detail information! Try Again');
        }
    }

    public function delete_document($id){
        $this->db->where('idDocument',$id);
        $query = $this->db->delete('document');

        if($query){
            return array('status' => "OK",'messsage' => 'Success delete document!');
        }else{
            return array('status' => "ERROR",'messsage' => 'Failed delete document! Try Again');
        }
    }
    
    public function get_status($id){
        $this->db->where('idAuth',$id);
        return $this->db->get('volunteer')->row()->status;
    }
    
    public function update_status_volunteer($idAuth,$status){
        $this->db->where('idAuth',$idAuth);
        return $this->db->update('volunteer',array('status'=>$status));
    }
    
    public function update_location_volunteer($idAuth,$lat,$long,$address){
        $this->db->where('idAuth',$idAuth);
        $query = $this->db->update('volunteer',array('latitude'=>$lat,'longitude'=>$long,'address'=>$address));
        if($query){
            return array('status' => "OK",'messsage' => 'Success update location!');
        }else{
            return array('status' => "ERROR",'messsage' => 'Failed update location! Try Again');
        }
        
    }

}
