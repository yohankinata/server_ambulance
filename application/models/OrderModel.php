<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OrderModel extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    public function get_order_user($id,$type=''){
        if($type=='volunteer'){
            $this->db->select('order_detail.idOrderDetail,order_detail.idOrder,datecreate,dateupdate,volunteer.displayName as volunter_name,volunteer.idVolunteer,order_detail.status as status_request,
            volunteer.handphone as volunteer_handphone,volunteer.photo as volunteer_photo,order_detail.notes_user');
            $this->db->join('order','order.idOrder = order_detail.idOrder');
            $this->db->join('volunteer','volunteer.idVolunteer = order_detail.idVolunteer');
            $this->db->where('order.idUser',$id);
    
            $this->db->order_by('datecreate','desc');
    
            $query = $this->db->get('order_detail');
        }else{
            $this->db->select('class_ambulance.*,order.*,ambulance.handphone as no_telp_ambulance,ambulance.nomorPolisi');
            $this->db->join('class_ambulance','class_ambulance.idClass = order.idClass','left');
            $this->db->join('ambulance','ambulance.idAmbulance = order.idAmbulance','left');
            $this->db->where('order.idUser',$id);
    
            $this->db->order_by('createdTime','desc');
    
            $query = $this->db->get('order');
        }
        

        if($query){
            return array('status' => "OK",'total_row'=> $query->num_rows(), 'result'=> $query->result());
        }else{
            return array('status' => "ERROR",'messsage' => 'Error get Data!');
        }
    }

    public function get_detail_order($id){
        $this->db->where('idOrder',$id);

        $query = $this->db->get('order');

        if($query){
            return array('status' => "OK",'result'=> $query->row());
        }else{
            return array('status' => "ERROR",'messsage' => 'Error get Data!');
        }
    }

    public function get_order_volunteer($id,$status=''){
        $this->db->select('user.displayName as user_order,user.photo as user_photo,order_detail.*,order.destination,order.no_hp as order_phone,longitude,latitude');
        if($status !=''){
            $this->db->where('order_detail.status',$status);
        }
        $this->db->where('order_detail.idVolunteer',$id);
        $this->db->order_by('createdtime','desc');

        $this->db->join('order','order.idOrder = order_detail.idOrder');
        $this->db->join('user','order.idUser = user.iduser');

        $query = $this->db->get('order_detail');

        if($query){
            return array('status' => "OK",'total_row'=> $query->num_rows(), 'result'=> $query->result());
        }else{
            return array('status' => "ERROR",'messsage' => 'Error get Data!');
        }
    }
    
    public function get_order_volunteer_detail($id){
        $this->db->select('user.displayName as user_order,order.destination,order.no_hp as order_phone');
        $this->db->where('order.idOrder',$id);
        $this->db->join('user','order.idUser = user.iduser');

        $query = $this->db->get('order');

        if($query){
            return array('status' => "OK", 'result'=> $query->row());
        }else{
            return array('status' => "ERROR",'messsage' => 'Error get Data!');
        }
    }

    public function get_class_ambulance($type=""){
        if($type !=""){
            $this->db->where('type',$type);
        }
        
        $query = $this->db->get('class_ambulance');

        if($query){
            return array('status' => "OK",'total_row'=> $query->num_rows(), 'result'=> $query->result());
        }else{
            return array('status' => "ERROR",'messsage' => 'Error get Data!');
        }
    }

    public function get_history_order($id){
        $this->db->where('idUser',$id);
        $this->db->where('order.idAmbulance is NOT NULL');
        $this->db->join('ambulance','ambulance.idAmbulance = order.idAmbulance');
        $this->db->join('class_ambulance','class_ambulance.idClass = order.idClass');
        
        $query = $this->db->get('order');

        if($query){
            return array('status' => "OK",'total_row'=> $query->num_rows(), 'result'=> $query->result());
        }else{
            return array('status' => "ERROR",'messsage' => 'Error get Data!');
        }
    }

    public function get_volunteer_available($latitude,$longtitude){
        $query = $this->db->query("SELECT idVolunteer,displayName,photo,handphone, latitude, longitude, address,
                111.045* DEGREES(ACOS(COS(RADIANS(latpoint))
                 * COS(RADIANS(latitude))
                 * COS(RADIANS(longpoint) - RADIANS(longitude))
                 + SIN(RADIANS(latpoint))
                 * SIN(RADIANS(latitude)))) AS distance_in_km
                 FROM volunteer
                 JOIN (
                     SELECT $latitude  AS latpoint,  $longtitude AS longpoint
                   ) AS p ON 1=1
                 WHERE latitude IS NOT NULL
                 ORDER BY distance_in_km
                 LIMIT 5");
        $query = $this->db->get('volunteer');
            
        if($query){
            return array('status' => "OK",'total_row'=>$query->num_rows(),'result' => $query->result());
        }else{
            return array('status' => "ERROR",'message'=>'Error get data');
        }
    }

    public function add_order($data){
        $check = $this->check_current_order($data['idUser']);
        if($check<1){
            $query = $this->db->insert('order',$data);
            if($query){
                return array('status' => "OK",'id_order'=> $this->db->insert_id(),'messsage' => 'Success to order! Please Wait!');
            }else{
                return array('status' => "ERROR",'messsage' => 'Failed to order! Try Again');
            }    
        }else{
            return array('status' => "ERROR",'messsage' => 'Failed to order! Order in process!');
        }
        
    }

    public function add_volunteer($data){
        $query = $this->db->insert('order_detail',$data);
        
        if($query){
            return array('status' => "OK",'messsage' => 'Success to request volunteer! Please Wait!');
        }else{
            return array('status' => "ERROR",'messsage' => 'Failed to request volunteer! Try Again');
        }
    }

    public function confirm_order($id,$status){
        $this->db->where('idOrderDetail',$id);
        $query = $this->db->update('order_detail',array('status'=>$status,'dateupdate'=>date("Y-m-d H:i:s")));

        if($query){
            return array('status' => "OK",'messsage' => 'Success confirm order!');
        }else{
            return array('status' => "ERROR",'messsage' => 'Failed confirm order! Try Again');
        }
    }

    public function done_order($id,$notes){
        $this->db->where('idOrder',$id);
        $query = $this->db->update('order',array('status'=>2,'notes_user'=>$notes));

        if($query){
            return array('status' => "OK",'messsage' => 'Success done order!');
        }else{
            return array('status' => "ERROR",'messsage' => 'Failed done order! Try Again');
        }
    }
    
    public function ok_order($id,$idclass,$idambulance){
        $this->db->where('idOrder',$id);
        $query = $this->db->update('order',array('status'=>1,'idAmbulance'=>$idambulance,'idClass'=>$idclass));

        if($query){
            return array('status' => "OK",'messsage' => 'Success ok order!');
        }else{
            return array('status' => "ERROR",'messsage' => 'Failed ok order! Try Again');
        }
    }

    public function cancel_order($id,$data){
        $data_order = $this->check_order($id);
        if($data_order->status == 0){
            $this->db->where('idOrder',$id);
            $query = $this->db->update('order',array('status'=>3,'notes_user'=>$data));

            if($query){
                return array('status' => "OK",'messsage' => 'Success to cancel order!');
            }else{
                return array('status' => "ERROR",'messsage' => 'Failed to cancel order! Try Again');
            }
        }else{
            return array('status' => "ERROR",'messsage' => 'Failed cancel order!');
        }
    }
    
    public function cancel_request($id,$data){
        $data_order = $this->check_detail($id);
        if($data_order->status == 0){
            $this->db->where('idOrderDetail',$id);
            $query = $this->db->update('order_detail',array('status'=>2,'dateupdate'=>date("Y-m-d H:i:s"),'notes_user'=>$data));

            if($query){
                return array('status' => "OK",'messsage' => 'Success to cancel request volunteer!');
            }else{
                return array('status' => "ERROR",'messsage' => 'Failed to cancel request volunteer! Try Again');
            }
        }else{
            return array('status' => "ERROR",'messsage' => 'Failed cancel request volunteer!');
        }
    }

    private function check_order($id){
        $this->db->where('idOrder',$id);
        return $this->db->get('order')->row();
    }
    
    private function check_detail($id){
        $this->db->where('idOrderDetail',$id);
        return $this->db->get('order_detail')->row();
    }

    private function check_volunteer($id){
        $this->db->where('idOrder',$id);
        return $this->db->get('order_detail');
    }

    private function check_current_order($id){
        $this->db->where('idUser = '.$id.' AND (status =0 OR status =1)');
        //$this->db->where('status',0);
        return $this->db->get('order')->num_rows();
    }
    
    private function get_volunteer_name($id){
        $this->db->select('displayName');
        $this->db->where('idVolunteer',$id);
        return $this->db->get('volunteer')->row()->displayName;
    }

    

}
