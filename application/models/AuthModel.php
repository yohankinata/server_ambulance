<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AuthModel extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    public function login($username='',$password=''){
        $this->db->select('username,password,idAuthentication,role,status,firebase_token');
        $this->db->where('username',$username);
        $this->db->where_not_in('status',0);
        $query = $this->db->get('authentication')->row();
        if($query){
            if($query->password == md5($password)){
               $created_at = date('Y-m-d H:i:s');
               $token = md5(uniqid());
               $this->db->trans_start();
               $this->db->insert('api_token',array('idAuth' => $query->idAuthentication,'API_Token' => $token,'createdDate'=>$created_at));
               if ($this->db->trans_status() === FALSE){
                  $this->db->trans_rollback();
                  return array('status' => "ERROR",'message' => 'Internal server error.');
               } else {
                  $this->db->trans_commit();
                  $photo = $this->get_photo($query->idAuthentication,$query->role);
                  if(!$photo){
                      $photo = "default.png";
                  }
                  return array('status' => "OK",'message' => 'Successfully login.','id' => $query->idAuthentication,'username'=>$query->username, 'token' => $token, 'role' => $query->role, 'profile'=>$query->status,'photo'=>$photo,'firebase_token'=>$query->firebase_token);
               }
            }
            return array('status' => "ERROR",'message' => 'Wrong Password');
        }else{
            return array('status' => "ERROR",'message' => 'Username not found.');
        }
    }

    public function logout($token)
    {
        $this->db->where('API_Token',$token)->delete('api_token');
        return array('status' => 'OK','message' => 'Successfully logout.');
    }

    public function auth($token)
    {
        $this->db->join('authentication','authentication.idAuthentication = api_token.idAuth');
        $q  = $this->db->select('createdDate,idAuth,role')->from('api_token')->where('API_Token',$token)->get()->row();
        if($q == ""){
            return array('status' => 'ERROR','message' => 'Unauthorized.');
        } else {
            if($q->createdDate > date('Y-m-d H:i:s', strtotime('+3 hours'))){
                return array('status' => 'ERROR','message' => 'Your session has been expired.');
            } else {
                return array('status' => 'OK','message' => 'Authorized.','id_auth'=>$q->idAuth,'role'=>$q->role);
            }
        }
    }

    public function delete_auth(){
        $data = array();
        $query = $this->db->get('api_token');

        foreach ($query->result() as $key => $value) {
            $date = strtotime($value->createdDate);
            $expired_at = strtotime('+12 hours',$date);
        
            if(date('Y-m-d H:i:s',$expired_at) < date('Y-m-d H:i:s')){
                array_push($data, $value->idToken);
            }else{
                continue;
            }
        }

        $check = array_filter($data);

        if(!empty($check)){
            $this->db->where_in('idToken',$data);
            $this->db->delete('api_token');
        }
    }

    public function register($data,$role,$password){
        $check = $this->check_username($data['username']);
        if($check > 0){
            return array('status' => 'ERROR','message' => 'Username Already Exist!');
        }else{
            $this->db->trans_start();
            $insert = $this->db->insert('authentication',$data);
            if($insert){
                $insert_user = "";
                if($role == "user"){
                    $data_user['iduser'] = '';
                    $data_user['idAuth'] = $this->db->insert_id();
                    $data_user['displayName'] = $data['username'];
                    $insert_user = $this->db->insert('user',$data_user);
                }else{
                    $data_user['idVolunteer'] = '';
                    $data_user['idAuth'] = $this->db->insert_id();
                    $data_user['displayName'] = $data['username'];
                    $data_user['status'] = 1;
                    $insert_user = $this->db->insert('volunteer',$data_user);
                }

                if($insert_user){
                    $config = array();
                    $config['protocol'] = 'smtp';
                    $config['smtp_host'] = 'mail.marketku.web.id';
                    $config['smtp_port'] = '25';
                    $config['smtp_user'] = 'admin@marketku.web.id';  //change it
                    $config['smtp_pass'] = '12345'; //change it
                    $config['charset'] = 'utf-8';
                    $config['newline'] = "\r\n";
                    $config['mailtype'] = 'html';
                    $config['wordwrap'] = TRUE;

                    $link = base_url().'index.php/auth/activation?username='.$data['username'];
                    $message = '<p>Horayy you already join with us..! please confirm this <a href="'. $link .'">link</a> to activation your account</p>
                        <p>account information </p>
                        <p>username : '.$data['username'].'</p>
                        <p>password : '.$password.'</p>
                        <b>Please save this information for login</b>';
                    $this->load->library('email',$config);
                    $this->email->from('admin@site.com','admin ambulance');
                    $this->email->to($data['email']);
                    $this->email->subject('Confirmation User Ambulance');
                    $this->email->message($message);

                    $this->email->send();
                    $this->db->trans_commit();
                    return array('status'=>'OK','message'=>'Success send message, please check your email address to complete registration');
                    
                }else{
                    $this->db->trans_rollback();
                    return array('status' => 'ERROR','message' => 'Failed Register, Something Wrong');
                }
            }else{
                $this->db->trans_rollback();
                return array('status' => 'ERROR','message' => 'Error Register! Please Try Again');
            }
        }
    }

    public function forgot_password($username){
        $this->db->trans_start();
        $this->db->where('username',$username);
        $check = $this->db->get('authentication');

        if($check->num_rows() >0){
            
            $config = array();
            $config['protocol'] = 'smtp';
            $config['smtp_host'] = 'mail.marketku.web.id';
            $config['smtp_port'] = '25';
            $config['smtp_user'] = 'admin@marketku.web.id';  //change it
            $config['smtp_pass'] = '12345'; //change it
            $config['charset'] = 'utf-8';
            $config['newline'] = "\r\n";
            $config['mailtype'] = 'html';
            $config['wordwrap'] = TRUE;

            $new_password = rand(1000,5000);
            
            $message = '<p>Forgot password success..! Here your new password <b>'. $new_password .'</b>, please save it well!</p>
            <p>account information </p>
            <p>username : '.$username.'</p>
            <p>password : '.$new_password.'</p>
            <b>Please save this information for login</b>';
            $this->load->library('email',$config);
            $this->email->from('admin@site.com','admin ambulance');
            $this->email->to($check->row()->email);
            $this->email->subject('Confirmation User Ambulance');
            $this->email->message($message);

            $this->email->send();
            $this->db->where('idAuthentication',$check->row()->idAuthentication);
            $update = $this->db->update('authentication',array('password'=>md5($new_password)));
            if($update){
                $this->db->trans_commit();
                return array('status'=>'OK','message'=>'Success send message, please check your email address to get new password');
            }else{
                $this->db->trans_rollback();
                return array('status'=> 'ERROR','message' => 'Failed forgot password!');
            }
            
        }else{
            return array('status'=> 'ERROR','message' => 'Username not found!');
        }
    }

    public function activation($username){
        $this->db->where('username',$username);
        $update =$this->db->update('authentication',array('status'=>1));
        if($update){
            return array('status' => 'OK','message' => 'Account success to activated');
        }else{
            return array('status'=> 'ERROR','message' => 'Account failed to activated');
        }
    }

    private function check_username($username){
        $this->db->where('username',$username);
        return $this->db->get('authentication')->num_rows();
    }
    
    public function update_token($idAuth,$token){
        if(!empty($token)){
            $check = $this->get_firebase_token_by_auth($idAuth)->row()->firebase_token;
            if($check != $token){
                $this->db->where('idAuthentication',$idAuth);
                $update =  $this->db->update('authentication',array('firebase_token'=>$token));
        
                if($update){
                    return array('status' => 'OK','message' => 'Token Success update');
                }else{
                    return array('status'=> 'ERROR','message' => 'Token Failed update');
                }        
            }else{
                return array('status' => 'OK','message' => 'Token Same');
            }    
        }else{
            return array('status' => 'OK','message' => 'No Token Parameter');
        }
        
    }
    
    public function get_firebase_volunteer_token($idVol){
        $this->db->where('idVolunteer',$idVol);
        $this->db->join('authentication','volunteer.idAuth = authentication.idAuthentication');
        return $this->db->get('volunteer');
    }
    
    public function get_firebase_user_token($idorder){
        $this->db->select('firebase_token,volunteer.displayName,volunteer.handphone');
        $this->db->where('idOrderDetail',$idorder);
        $this->db->join('order','order_detail.idOrder = order.idOrder');
        $this->db->join('volunteer','order_detail.idVolunteer = volunteer.idVolunteer');
        $this->db->join('user','order.idUser = user.idUser');
        $this->db->join('authentication','user.idAuth = authentication.idAuthentication');
        return $this->db->get('order_detail');
    }
    
    private function get_firebase_token_by_auth($idAuth){
        $this->db->where('idAuthentication',$idAuth);
        return $this->db->get('authentication');
    }
    
    private function get_photo($id,$type){
        $this->db->where('idAuth',$id);
        if($type =='volunteer'){
            return $this->db->get('volunteer')->row()->photo;
        }else{
            return $this->db->get('user')->row()->photo;
        }
    }

}
