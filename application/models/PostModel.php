<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PostModel extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    public function get_all_post($cat=""){
        
        if($cat != ""){
            $this->db->join('post_to_category','post.idPost = post_to_category.idPost');
            $this->db->where('idCategory',$cat);
        }

        $query = $this->db->get('post');

        if($query){
            $all_post = array();
            foreach ($query->result() as $key => $value) {
                $post = array();
                $post['id'] = $value->idPost;
                $post['title'] = $value->title;
                $post['thumbnail'] = $value->thumbnail;
                $post['postDate'] = $value->postDate;
                $post['content'] = $value->content;
                $post['createdTime'] = $value->createdTime;
                $post['createdBy'] = $value->createdBy;
                $post['updateTime'] = $value->updateTime;
                $post['updateBy'] = $value->updateBy;
                $post['author'] = $value->author;
                $post['category'] = $this->get_category($value->idPost);
                array_push($all_post, $post);
            }

            return array('status' => "OK",'total_row'=> $query->num_rows(), 'result'=> $all_post);
        }else{
            return array('status' => "ERROR",'messsage' => 'Error get Data!');
        }
    }

    public function get_all_category(){
        
        // $this->db->join('category','post_to_category.idCategory = category.idCategory');

        $query = $this->db->get('category');

        if($query){
            return array('status' => "OK",'total_row'=> $query->num_rows(), 'result'=> $query->result());
        }else{
            return array('status' => "ERROR",'messsage' => 'Error get Data!');
        }
    }

    private function get_category($id){
        $this->db->select('name');
        $this->db->join('category','post_to_category.idCategory = category.idCategory');
        $this->db->where('idPost',$id);
        $query = $this->db->get('post_to_category');

        if($query){
            $category = array();
            foreach ($query->result() as $key => $value) {
                array_push($category, $value->name);
            }
            return $category;
        }else{
            return array('status' => "ERROR",'messsage' => 'Error get Data!');
        }
    }

    public function detail_post($id){
        $this->db->where('idPost',$id);
        // $this->db->join('post_to_category','post.idPost = post_to_category.idCategory');
        // $this->db->join('category','post_to_category.idCategory = category.idCategory');

        $query = $this->db->get('post');

        if($query){
            return array('status' => "OK",'result'=> $query->row());
        }else{
            return array('status' => "ERROR",'messsage' => 'Error get Data!');
        }
    }

}
