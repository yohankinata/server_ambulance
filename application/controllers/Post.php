<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';


class Post extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model('AuthModel');
        $this->load->model('UserModel');
        $this->load->model('PostModel');
    }

    public function index_get($method='')
    {   
        $token = $this->get('token');
        $response = $this->AuthModel->auth($token);
        if($response['status'] == 'OK'){
            if($method=='all'){
                $cat = $this->get('cat');
                $get_post = $this->PostModel->get_all_post($cat);
                $this->response($get_post);
            }else if($method=='detail'){
                $id_post = $this->get('id_post');
                $get_detail = $this->PostModel->detail_post($id_post);
                $this->response($get_detail);
            }else if($method=='category'){
                $get_category = $this->PostModel->get_all_category();
                $this->response($get_category);;
            }else{
                $this->response(array("status"=> FALSE,"result"=> "Bad Request"),400);
            }
        }else{
            $this->response($response,200);
        }
    }

}
