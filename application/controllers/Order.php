<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';


class Order extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model('AuthModel');
        $this->load->model('UserModel');
        $this->load->model('OrderModel');
    }

    public function index_get($method='',$type='')
    {   
        $token = $this->get('token');
        $response = $this->AuthModel->auth($token);
        if($response['status'] == 'OK'){
            if($method=='order_user'){
                $id_user = $this->UserModel->get_id_by_auth($response['id_auth'],'user');
                $order_user = $this->OrderModel->get_order_user($id_user,$type);
                $this->response($order_user,200);
            }else if($method=='order_volunteer'){
                $id_volunteer = $this->UserModel->get_id_by_auth($response['id_auth'],'volunteer');
                $order_volunteer = $this->OrderModel->get_order_volunteer($id_volunteer,$type);
                $this->response($order_volunteer,200);
            }else if($method=='order_history'){
                $id_user = $this->UserModel->get_id_by_auth($response['id_auth'],'user');
                $order_history = $this->OrderModel->get_history_order($id_user);
                $this->response($order_history,200);;
            }else if($method=='class_ambulance'){
                $class_ambulance = $this->OrderModel->get_class_ambulance($type);
                $this->response($class_ambulance,200);;
            }else if($method=='volunteer'){
                $id_order = $this->get('id_order');
                $detail = $this->OrderModel->get_detail_order($id_order);
                $volunteer = $this->OrderModel->get_volunteer_available($detail['result']->latitude,$detail['result']->longitude);
                $this->response($volunteer,200);;
            }else{
                $this->response(array("status"=> FALSE,"result"=> "Bad Request"),400);
            }
        }else{
            $this->response($response,200);
        }
    }

    public function index_post($method='')
    {
        $token = $this->input->get('token');
        $id_order = $this->post('id_order');
        $response = $this->AuthModel->auth($token);
        if($response['status'] == 'OK'){
            if($method == 'add'){
                $id_user = $this->UserModel->get_id_by_auth($response['id_auth'],'user');
                $data = array(
                    "idOrder"=>'',
                    "idAmbulance"=>'',
                    "idUser"=>$id_user,
                    "idClass"=>$this->post('id_class'),
                    "createdTime"=> date('Y-m-d H:i:s'),
                    "status"=>0,
                    "paymentStatus"=> "Belum Lunas",
                    "destination"=>$this->post('destination'),
                    "no_hp"=> $this->post('phone'),
                    "longitude"=>$this->post('longitude'),
                    "latitude"=>$this->post('latitude')
                );

                $add_order = $this->OrderModel->add_order($data);

                $this->response($add_order,200);
            }else if ($method == 'request'){
                $id_order = $this->post('id_order');
                $id_volunteer = explode(',',$this->post('id_volunteer'));
                
                $push = $this->OrderModel->get_order_volunteer_detail($id_order);
                if($push['status']=='OK'){
                    $order = $push['result'];
                    for($i=0;$i<count($id_volunteer);$i++){
                        $data = array(
                            "idOrderDetail"=>'',
                            "idOrder"=>$id_order,
                            "idVolunteer"=>$id_volunteer[$i],
                            "datecreate"=>date("Y-m-d H:i:s")
                        );
                        $request = $this->OrderModel->add_volunteer($data);
                    
                        $firebase_token = $this->AuthModel->get_firebase_volunteer_token($id_volunteer[$i])->row()->firebase_token;
                        
                        $this->load->model('PushModel');
                        
                        $title = "URGENT-REQUEST VOLUNTEER";
                        $message = "Anda menerima panggilan voluntir darurat atas nama " . $order->user_order .", ".$order->order_phone.
                        ", ". $order->destination;

                        $this->PushModel->setTitle($title);
                        $this->PushModel->setMessage($message);
                        $this->PushModel->setIsBackground(TRUE);
                        $this->PushModel->setPayload($push['result']);
                        $json = $this->PushModel->getPush();
                        $response = $this->PushModel->send($firebase_token,$json);
                    }
                    $this->response($request,200);
                }else{
                    $this->response($push,200);
                }

            }else if ($method == 'confirm'){
                $get_status = $this->UserModel->get_status($response['id_auth']);
                if($get_status == 1){
                    $id_order = $this->post('id_order_detail');
                    $confirm_order = $this->OrderModel->confirm_order($id_order,1);
                    $update_status_volunteer = $this->UserModel->update_status_volunteer($response['id_auth'],0);
                    if($update_status_volunteer){
                        $user = $this->AuthModel->get_firebase_user_token($id_order)->row();
                        
                        $firebase_token = $user->firebase_token;
                        
                        $this->load->model('PushModel');
                        
                        $title = "VOLUNTEER-ACCEPT REQUEST";
                        $message = "Request anda diterima oleh volunteer atas nama " . $user->displayName .", ".$user->handphone;

                        $this->PushModel->setTitle($title);
                        $this->PushModel->setMessage($message);
                        $this->PushModel->setIsBackground(TRUE);
                        $json = $this->PushModel->getPush();
                        $response = $this->PushModel->send($firebase_token,$json);
                        $this->response($confirm_order,200);
                    }else{
                        $this->response(array("status"=>"ERROR","message"=>"Gagal update status voluntir!"),200);
                    }
                }else{
                    $this->response(array("status"=>"ERROR","message"=>"Masih terlibat pada pesanan!"),200);
                }
                
            }else if ($method == 'done'){
                $id_order = $this->post('id_order');
                $notes = $this->post('notes');
                $done_order = $this->OrderModel->done_order($id_order,$notes);
                $this->response($done_order,200);
            }else if ($method == 'done_volunteer'){
                $id_order = $this->post('id_order_detail');
                $done_order_detail = $this->OrderModel->confirm_order($id_order,3);
                $update_status_volunteer = $this->UserModel->update_status_volunteer($response['id_auth'],1);
                $this->response($done_order_detail,200);
            }else if ($method == 'ok_order'){
                $id_order = $this->post('id_order');
                $done_order = $this->OrderModel->ok_order($id_order,2,1);
                $this->response($done_order,200);
            }else if ($method == 'cancel_order'){
                $cancel_order = $this->OrderModel->cancel_order($id_order,$this->post('reason'));
                $this->response($cancel_order,200);
            }else if ($method == 'cancel_request'){
                $cancel_request = $this->OrderModel->cancel_request($this->post('id_order_detail'),$this->post('reason'));
                if($response['role']=="volunteer"){
                    $update_status_volunteer = $this->UserModel->update_status_volunteer($response['id_auth'],1);
                }
                $this->response($cancel_request,200);
            }else{
                $this->response(array('status' => 'ERROR','message' => 'Bad request.'),400);
            }
        }else{
            $this->response($response,200);
        }
        
    }

}
