<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';


class User extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model('AuthModel');
        $this->load->model('UserModel');
        $this->load->model('OrderModel');
    }

    public function index_get($method='')
    {   
        $token = $this->get('token');
        $response = $this->AuthModel->auth($token);
        if($response['status'] == 'OK'){
            if($method=='self'){
                $get_self = $this->UserModel->get_self_profile($response['id_auth'],$response['role']);
                $this->response($get_self);
            }else if($method=='profile'){
                $idAuth = $this->get('id');
                $get_profile = $this->UserModel->get_user_profile($idAuth);
                $this->response($get_profile);
            }else if($method=='near_volunteer'){
                $lat = $this->get('latitude');
                $long = $this->get('longitude');
                $get_volunter = $this->OrderModel->get_volunteer_available($lat,$long);
                $this->response($get_volunter,200);
            }else{
                $this->response(array("status"=> "ERROR","result"=> "Bad Request"),400);
            }
        }else{
            $this->response($response,200);
        }
    }

    public function index_post($method='',$action='')
    {
        $token = $this->input->get('token'); 
        $response = $this->AuthModel->auth($token);
        if($response['status'] == 'OK'){
            if($method == 'update'){
                $password = $this->post('password');
                //$email = $this->post('email');
                
                $update = array();
                
                $update_auth = array();

                if(!empty($password)){
                    $update_auth["password"] = md5($password);
                }

                //$update_auth["email"] = $email;
                $update_auth["status"] = 2;

                $update["displayName"] = $this->post('displayName');
                $update["dateofBirth"] = $this->post('dateofBirth');
                $update["golonganDarah"] = $this->post('golonganDarah');
                $update["address"] = $this->post('address');
                $update["tinggiBadan"] = $this->post('tinggiBadan');
                $update["handphone"] = $this->post('handphone');
                $update["biografi"] = $this->post('biografi');
                $update["riwayatKesehatan"] = $this->post('riwayatKesehatan');
                
                if($response['role'] == 'volunteer'){
                    $update['status'] = 1;
                    if(!empty($this->post('status'))){
                        $update["status"] = $this->post('status');    
                    }
                }

                $update_profile = $this->UserModel->update_profile($response['id_auth'],$response['role'],$update_auth,$update);
                $this->response($update_profile,200);
                
            }else if ($method == 'update_photo'){
                $id = $this->UserModel->get_id_by_auth($response['id_auth'],$response['role']);
                $status = FALSE;
                $update = array();
                $message = "Belum ada foto yang diupload";
                $photo = $this->post('photo');
                $type = $this->post('type');
                $path = realpath(APPPATH . '../asset/photo/');
                    
                if(isset($photo) && isset($type)){
                    if(($type == 'png')||($type == 'jpg')||($type == 'jpeg')||($type == 'PNG')||($type == 'JPG')||($type == 'JPEG')){
                        $img = str_replace(' ', '+', $photo);
                    	$data = base64_decode($img);
                    	$file = $path.'/'.$response['id_auth'].'.'.$type;
                        $success = file_put_contents($file, $data);
                    	
                        if($success){
                            $update["photo"]= $response['id_auth'].'.'.$type;
                            $status = TRUE;
                        }else{
                            $status = FALSE;
                            $message = "Gagal upload foto";
                        }
                    }else{
                        $status = FALSE;
                        $message = "photo type not allowed";
                    }
                }
                
                if($status == TRUE){
                    $update_photo = $this->UserModel->update_photo($response['id_auth'],$response['role'],$update);
                    $this->response($update_photo,200);
                }else{
                    $this->response(array('status'=>'ERROR','message'=>$message),200);
                }
                
            }else if ($method == 'update_location'){
                $update_location = $this->UserModel->update_location_volunteer($response['id_auth'],$this->post('latitude'),$this->post('longitude'),$this->post('address'));;
                $this->response($update_location,200);
            }else if ($method == 'update_status'){
                $update_status = $this->UserModel->update_status_volunteer($response['id_auth'],$this->post('status'));;
                if($update_status){
                    $this->response(array("status"=>"OK","messsage"=>"Berhasil update status"),200);    
                }else{
                    $this->response(array("status"=>"ERROR","messsage"=>"Gagal update status"),200);
                }
                
            }else if ($method == 'update_detail'){
                $id = $this->UserModel->get_id_by_auth($response['id_auth'],$response['role']);
                $detail = array();
                if($action =='tekanan_darah'){
                    $detail['sistol'] = $this->post('sistol');
                    $detail['diastol'] = $this->post('diastol');
                    $detail['nadi'] = $this->post('nadi');
                }else{
                    $detail['nilai'] = $this->post('nilai');
                    $detail['date'] = date('Y-m-d H:i:s');
                }

                $update_detail = $this->UserModel->update_detail($id,$action,$detail);
                $this->response($update_detail,200);
            }else if ($method == 'delete_detail'){
                $id = $this->UserModel->get_id_by_auth($response['id_auth'],$response['role']);
                $delete_detail = $this->UserModel->delete_detail($id,$action);
                $this->response($delete_detail,200);
            }else if ($method == 'document'){
                if($action =='add'){
                    $id = $this->UserModel->get_id_by_auth($response['id_auth'],$response['role']);
                    $data = array();
                    $doc = "";
                    $document = array();
                    $data['idVolunteer'] = $id;
                    $data['name'] = $this->post('name');
                    $data['description'] = $this->post('description');
                    
                    $path = realpath(APPPATH . '../asset/document/');
                    
                    $doc = $this->post('doc');
                    $type = $this->post('type');
                    if(isset($doc) && isset($type)){
                        if(($type == 'png')||($type == 'jpg')||($type == 'jpeg')||($type == 'PNG')||($type == 'JPG')||($type == 'JPEG')||($type == 'doc')||($type == 'docx')||($type == 'pdf')){
                            $docx = str_replace(' ', '+', $doc);
                        	$data_doc = base64_decode($docx);
                        	$file = $path.'/'.$response['id_auth'].'_'.str_replace(' ', '', $this->post('name')) .'.'.$type;
                        	$success = file_put_contents($file, $data_doc);
                        	
                            if($success){
                                $data['path'] = $response['id_auth'].'_'.str_replace(' ', '', $this->post('name')) .'.'.$type;
                                $document = $this->UserModel->add_document($data); 
                            }else{
                                $document['status'] = "ERROR";
                                $document['message'] = "Gagal upload document!";
                            }
                        }else{
                            $document['status'] = "ERROR";
                            $document['message'] = "document type not allowed";
                        }
                    }
                    
                    $this->response($document,200);
                    
                }else if($action =='delete'){
                    $id = $this->post('id_doc');

                    $path = $this->UserModel->get_single_document($id);
                    unlink('asset/document/'.$path);

                    $document = $this->UserModel->delete_document($id);
                    $this->response($document,200);
                }else{
                    $this->response(array('status' => 'ERROR','message' => 'Bad request.'),400);
                }
            }else{
                $this->response(array('status' => 'ERROR','message' => 'Bad request.'),400);
            }
        }else{
            $this->response($response,200);
        }
    }

}
