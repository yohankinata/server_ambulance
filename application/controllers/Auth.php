<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';


class Auth extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model('AuthModel');
    }

    public function index_get($method='')
    {
        if($method=='activation'){
            $activation = $this->AuthModel->activation($this->get('username'));
            $this->response($activation);
        }else{
            $this->response(array("status"=> FALSE,"result"=> "Bad Request"),400);
        }
    }

    public function index_post($method='',$role='')
    {
        $token = $this->input->get('token'); 
        $username = $this->post('username');
        $password = $this->post('password');
        
        if($method == 'login'){
            $response_login = $this->AuthModel->login($username,$password);
            $this->response($response_login,200);

        }else if ($method == 'logout'){
            $response_logout = $this->AuthModel->logout($token);
            $this->response($response_logout,200);
        }else if ($method == 'auth'){
            $response_auth = $this->AuthModel->auth($token);
            $this->response($response_auth,200);
        }else if ($method == 'register'){
            $email = $this->post('email');
            $data = array(
                'idAuthentication'=>'',
                'username'=>$username,
                'password'=>md5($password),
                'email'=>$email,
                'role'=>$role
                );
            if(($role == "user") || ($role == 'volunteer')){
                $register = $this->AuthModel->register($data,$role,$password);
                $this->response($register);
            }else{
                $this->response(array('status' => 'ERROR','message' => 'Bad request.'),400);
            }
        }else if ($method == 'forgot'){
            $response_forgot = $this->AuthModel->forgot_password($username);
            $this->response($response_forgot,200);
        }else if ($method == 'update_token'){
            $idAuth = $this->post('id_auth');
            $firebase_token = $this->post('firebase_token');
            $response_token = $this->AuthModel->update_token($idAuth,$firebase_token);
            $this->response($response_token,200);
        }else{
            $this->response(array('status' => 'ERROR','message' => 'Bad request.'),400);
        }
    }

}
